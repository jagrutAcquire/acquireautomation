package loadgenerator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.acq.qa.base.TestBase;
import com.acq.qa.util.CommonUtils;
import com.github.javafaker.Faker;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LoadGenerate5 extends TestBase{
	public static CommonUtils commonUtil;
	static Faker faker = new Faker();

	public static void explicitwait(String xpath) {

		WebDriverWait w = new WebDriverWait(driver, 160);
		w.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

	}

	public static void loginMonday() throws Exception {

		int i = 0;
		for (i = 0; i < 1000; i++) {
			String xpathForLauncherFrame = ".//iframe[@name='aio-launcher-frame']";
			String xpathforAoiLaucnher = "//div[@id='aio-launcher-container']";
			String xpathforFrame2 = "//iframe[contains(@class, 'tagove_frame')]";
			String xpathforHome = ".//div[@class='home-board-footer']";
			String xpathforFname = ".//input[starts-with(@id,'form-contact.name')]";
			String xpathforEmail = ".//input[starts-with(@id,'form-contact.email')]";
			String xpathforHomebutton = ".//button[@class='home-board-button']";
			String xpathforTextarea = ".//textarea[@class='textarea __text__editor']";
			String fName = faker.name().fullName();
			String comapnyName = faker.company().name().replaceAll("[\\s\\'\\.\\^:,]", "").toLowerCase();
			String firstName = fName.replaceAll("[\\s\\'\\.\\^:,]", "").toLowerCase();

			System.out.println("i Value is :: " + i);
			Map<String, Object> prefs = new HashMap<String, Object>();
			WebDriverManager.chromedriver().setup();
			prefs.put("profile.default_content_setting_values.notifications", 2);
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			options.setExperimentalOption("prefs", prefs);
			options.addArguments("--disable-extensions");
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			options.setExperimentalOption("useAutomationExtension", false);
			options.addArguments("use-fake-ui-for-media-stream");
			//options.addArguments("--incognito");
			driver = new ChromeDriver(options);
			// driver.manage().timeouts().pageLoadTimeout(CommonUtils.PAGE_LOAD_TIMEOUT,
			// TimeUnit.SECONDS);
			// driver.manage().timeouts().implicitlyWait(CommonUtils.IMPLICIT_WAIT,
			// TimeUnit.SECONDS);
			 //driver.get("https://39337y.acquire.io/simulate");
			driver.get("https://3inc19.preprod.env.acquire.io/simulate");
			//driver.get("https://c4rrie.uat.env.acquire.io/simulate");
			// driver.get("https://43pfzc.uat.env.acquire.io/simulate");
			//driver.get("https://ti08fw.acquire.io/simulate");
			//driver.get("https://43pfzc.uat.env.acquire.io/simulate");
			// driver.get("https://a7s2ks.acquire.io/simulate");
			// driver.get("https://ri17rq.uat.env.acquire.io/simulate");
			explicitwait(xpathForLauncherFrame);
			driver.switchTo().frame(driver.findElement(By.xpath(xpathForLauncherFrame)));
			explicitwait(xpathforAoiLaucnher);
			driver.findElement(By.xpath(xpathforAoiLaucnher)).click();
			driver.switchTo().defaultContent();
			Thread.sleep(300);
			explicitwait(xpathforFrame2);
			driver.switchTo().frame(driver.findElement(By.xpath(xpathforFrame2)));
			explicitwait(xpathforHome);
			driver.findElement(By.xpath(xpathforHome)).click();
			explicitwait(xpathforFname);
			WebElement firstname = driver.findElement(By.xpath(xpathforFname));
			firstname.sendKeys(fName);
			explicitwait(xpathforEmail);
			WebElement emailadd = driver.findElement(By.xpath(xpathforEmail));
			emailadd.sendKeys(firstName + "@" + comapnyName + ".com");
			Thread.sleep(200);

			driver.findElement(By.xpath(xpathforHomebutton)).click();
			Thread.sleep(500);
			explicitwait(xpathforTextarea);
			driver.findElement(By.xpath(xpathforTextarea)).sendKeys(faker.address().country()
					+ faker.address().streetName() + faker.address().secondaryAddress()
					+ faker.address().streetAddress() + faker.address().buildingNumber() + faker.address().fullAddress()
					+ faker.address().zipCode() + faker.address().countryCode() + faker.book().author()
					+ faker.book().publisher() + faker.book().title() + faker.book().genre() + faker.ancient().god()
					+ faker.ancient().hero() + faker.superhero().name() + faker.beer().name()
					+ faker.commerce().department() + faker.commerce().productName() + faker.color().hashCode()
					+ faker.color().name() + faker.color().hex() + faker.date().birthday() + faker.dog().name()
					+ faker.food().spice() + faker.friends().location() + faker.internet().domainName()
					+ faker.internet().domainSuffix() + faker.internet().ipV4Address() + faker.commerce().material()
					+ faker.address().city() + Keys.ENTER);
			Thread.sleep(200);
			driver.close();
			driver.quit();
			//Runtime.getRuntime().exec("taskkill /f /im chromedriver.exe");
		}

		

		
	}

	public static void main(String[] args) throws Exception {

		loginMonday();

	}

}
