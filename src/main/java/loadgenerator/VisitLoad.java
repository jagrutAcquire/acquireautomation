package loadgenerator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class VisitLoad {

	public static WebDriver driver;
	private static String[] Beginning = { "Kr", "Ca", "Ra", "Mrok", "Cru", "Ray", "Bre", "Zed", "Drak", "Mor", "Jag",
			"Mer", "Jar", "Mjol", "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro", "Mar", "Luk" };
	private static String[] Middle = { "air", "ir", "mi", "sor", "mee", "clo", "red", "cra", "ark", "arc", "miri",
			"lori", "cres", "mur", "zer", "marac", "zoir", "slamar", "salmar", "urak" };
	private static String[] End = { "d", "ed", "ark", "arc", "es", "er", "der", "tron", "med", "ure", "zur", "cred",
			"mur" };

	private static Random rand = new Random();

	public static String generateName() {

		return Beginning[rand.nextInt(Beginning.length)] + Middle[rand.nextInt(Middle.length)]
				+ End[rand.nextInt(End.length)];

	}

	public static void explicitwait(String xpath) {

		WebDriverWait w = new WebDriverWait(driver, 50);
		w.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

	}

	public static void loginMonday() throws Exception {
		System.out.println("randome name:  " + generateName());

		int i = 0;
		for (i = 0; i < 3000; i++) {
			String xpathForLauncherFrame = ".//iframe[@name='aio-launcher-frame']";
			String xpathforAoiLaucnher = "//div[@id='aio-launcher-container']";
			String xpathforFrame2 = "//iframe[contains(@class, 'tagove_frame')]";
			String xpathforHome = ".//div[@class='home-board-footer']";
			String xpathforFname = ".//input[starts-with(@id,'form-contact.name')]";
			String xpathforEmail = ".//input[starts-with(@id,'form-contact.email')]";
			String xpathforHomebutton = ".//button[@class='home-board-button']";
			String xpathforTextarea = ".//textarea[@class='textarea __text__editor']";
			String fName = generateName();
			String lName = generateName();
			String comapnyName = generateName().replaceAll("[\\s\\'\\.\\^:,]", "").toLowerCase();
			String firstName = fName.replaceAll("[\\s\\'\\.\\^:,]", "").toLowerCase();
			System.out.println("i Value is :: " + i);
			Map<String, Object> prefs = new HashMap<String, Object>();
			//WebDriverManager.chromedriver().setup();
			 // System Property for Chrome Driver   
	        System.setProperty("webdriver.chrome.driver", "C://Automation Framework//acquireautomation//executables//chromedriver.exe");  
	          
	             // Instantiate a ChromeDriver class.     
	        
			prefs.put("profile.default_content_setting_values.notifications", 2);
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			ChromeOptions options = new ChromeOptions();
		//	options.addArguments("--headless");
			options.setExperimentalOption("prefs", prefs);
			options.addArguments("--disable-extensions");
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			options.setExperimentalOption("useAutomationExtension", false);
			options.addArguments("use-fake-ui-for-media-stream");
			options.addArguments("--incognito");
			driver = new ChromeDriver(options);

			driver.get("https://3inc19.preprod.env.acquire.io/simulate");

			explicitwait(xpathForLauncherFrame);
			driver.switchTo().frame(driver.findElement(By.xpath(xpathForLauncherFrame)));
			explicitwait(xpathforAoiLaucnher);
			driver.findElement(By.xpath(xpathforAoiLaucnher)).click();
			driver.switchTo().defaultContent();
			explicitwait(xpathforFrame2);
			driver.switchTo().frame(driver.findElement(By.xpath(xpathforFrame2)));
			explicitwait(xpathforHome);
			Thread.sleep(5000);
			driver.findElement(By.xpath(xpathforHome)).click();
			explicitwait(xpathforFname);
			Thread.sleep(300);
			WebElement firstname = driver.findElement(By.xpath(xpathforFname));
			firstname.sendKeys(fName + " "+lName);
			explicitwait(xpathforEmail);
			WebElement emailadd = driver.findElement(By.xpath(xpathforEmail));
			emailadd.sendKeys(firstName + "@" + comapnyName + ".com");
			Thread.sleep(300);
			driver.findElement(By.xpath(xpathforHomebutton)).click();
			Thread.sleep(300);
			explicitwait(xpathforTextarea);
			driver.findElement(By.xpath(xpathforTextarea)).sendKeys("Hello This is" + fName + " Here " + Keys.ENTER);
			Thread.sleep(200);
			driver.close();
			driver.quit();
		}

	}

	public static void main(String[] args) throws Exception {

		loginMonday();

	}

}
