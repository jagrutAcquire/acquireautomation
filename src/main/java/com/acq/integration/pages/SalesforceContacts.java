package com.acq.integration.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.acq.qa.base.TestBase;
import com.acq.qa.util.Variables;
import com.relevantcodes.extentreports.LogStatus;

public class SalesforceContacts extends TestBase {

	By APPSTORE = By.xpath(Variables.OR.getProperty("menuAppStore"));
	By APPSTORE_MENU_TITLE = By.xpath(Variables.OR.getProperty("appstTitleDisplay"));
	By APPSTORE_All_COLLECTION = By.xpath(Variables.OR.getProperty("appstAllCollection"));
	By SF_TITLE = By.xpath(Variables.OR.getProperty("salesforceDisplay"));
	By SF_SETTING = By.xpath(Variables.OR.getProperty("salesforceSetting"));
	By SF_MAPPING = By.xpath(Variables.OR.getProperty("saleFConversion"));
	By SF_Contact = By.xpath(Variables.OR.getProperty("salesFContact"));
	By SF_SYNC_SETTING = By.xpath(Variables.OR.getProperty("salesFSyncSetting"));
	By CONFIRM_MAPPING_BTN = By.xpath(Variables.OR.getProperty("confirmMappingButton"));
	By SF_CASE = By.xpath(Variables.OR.getProperty("salesFCase"));
	By SF_NOTE = By.xpath(Variables.OR.getProperty("salesFNote"));
	By SF_FEED = By.xpath(Variables.OR.getProperty("salesFFeed"));
	By SF_SAVE_BTN = By.xpath(Variables.OR.getProperty("salesFSaveSyncBtn"));
	By SFUSER_NAME = By.id("username");
	By SFPASSWORD = By.id("password");
	By SF_LOGINBTN = By.id("Login");
	By SF_Sync_BTN = By.id("Salesforce-Default");
	By CLOSE_CHAT = By.cssSelector(Variables.OR.getProperty("dashCloseChat"));
	By HOME_MENU = By.xpath(Variables.OR.getProperty("menuHome"));

	public SalesforceContacts() {
		PageFactory.initElements(driver, this);
	}

	public void Salesforcelogin(String userName, String password) {

		try {
			commonUtil.waitForElementToBeVisible(SFUSER_NAME);
			commonUtil.sendKeys(SFUSER_NAME, userName);
			commonUtil.waitForElementToBeVisible(SFPASSWORD);
			commonUtil.sendKeys(SFPASSWORD, password);
			commonUtil.clickButton(SF_LOGINBTN);
			test.log(LogStatus.INFO, "Salesforce Login Successfully");

		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Salesforce Login Failed ");
			throw new AssertionError("Salesforce Login Failed", e);
		}
	}

	public String validateSalesforceTitle() {

		return driver.getTitle();
	}

	public void clickappstoreMenu() {

		try {
			commonUtil.waitForElementToBeVisible(APPSTORE);
			commonUtil.clickButton(APPSTORE);
			test.log(LogStatus.INFO, "App store Menu Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Unable to click on App store Menu");
			throw new AssertionError("Unable to click on App store Menu", e);
		}
	}
	
	public void clickHomeMenu() {

		try {
			commonUtil.waitForElementToBeVisible(HOME_MENU);
			commonUtil.clickButton(HOME_MENU);
			test.log(LogStatus.INFO, "Home Menu Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Unable to click on Home");
			throw new AssertionError("Unable to click on Home", e);
		}
	}

	public void clickAllCollection() {

		try {
			commonUtil.waitForElementToBeVisible(APPSTORE_All_COLLECTION);
			commonUtil.clickButton(APPSTORE_All_COLLECTION);
			test.log(LogStatus.INFO, "App store All collection sub Menu Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Faild to Click on App store All collection sub Menu");
			throw new AssertionError("Faild to Click on App store All collection sub Menu", e);
		}
	}

	public void clickSFSetting() {

		try {
			Thread.sleep(2000);
			commonUtil.waitForElementToBeVisible(SF_SETTING);
			commonUtil.clickButton(SF_SETTING);
			test.log(LogStatus.INFO, "App store Setting Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Faild to Click on App store Setting Clicked");
			throw new AssertionError("Faild to Click on App store Setting Clicked", e);
		}

	}

	public void clickMappingFieldsMenu() {

		try {
			commonUtil.waitForElementToBeVisible(SF_MAPPING);
			commonUtil.clickButton(SF_MAPPING);
			test.log(LogStatus.INFO, "Mapping Field in Salesforce Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Faild to Click on Mapping Field in Salesforce");
			throw new AssertionError("Faild to Click on Mapping Field in Salesforce", e);
		}

	}

	public void clickContact() {

		try {
			commonUtil.waitForElementToBeVisible(SF_Contact);
			commonUtil.clickButton(SF_Contact);
			test.log(LogStatus.INFO, "Salesforce Contact Field click PASS");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Salesforce Contact Field click FAIL");
			throw new AssertionError("Salesforce Contact Field click FAIL", e);
		}

	}

	public void confirmMapping() {

		try {
			commonUtil.waitForElementToBeVisible(CONFIRM_MAPPING_BTN);
			commonUtil.clickButton(CONFIRM_MAPPING_BTN);
			test.log(LogStatus.INFO, "Salesforce Confirm Mapping Button click PASS");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Salesforce Confirm Mapping Button click FAIL");
			throw new AssertionError("Salesforce Confirm Mapping Button FAIL", e);
		}
	}

	public void clickSyncSetting() {

		try {
			Thread.sleep(1000);
			commonUtil.waitForElementToBeVisible(SF_SYNC_SETTING);
			commonUtil.clickButton(SF_SYNC_SETTING);
			test.log(LogStatus.INFO, "Sync Setting in Salesforce Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Faild to Click on Sync Setting in Salesforce");
			throw new AssertionError("Faild to Click on Sync Setting in Salesforce", e);
		}

	}

	public void clickcase() {

		try {

			commonUtil.waitForElementToBeVisible(SF_CASE);
			commonUtil.clickButton(SF_CASE);
			test.log(LogStatus.INFO, "CASE in Sync Setting Salesforce Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Faild to Click on CASE in Sync Setting Salesforce");
			throw new AssertionError("Faild to Click on CASE in Sync Setting Salesforce", e);
		}

	}

	public void clickNote() {

		try {
			commonUtil.waitForElementToBeVisible(SF_NOTE);
			commonUtil.clickButton(SF_NOTE);
			test.log(LogStatus.INFO, "NOTE in Sync Setting Salesforce Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Faild to Click on NOTE in Sync Setting Salesforce");
			throw new AssertionError("Faild to Click on NOTE in Sync Setting Salesforce", e);
		}

	}

	public void clickFeed() {

		try {
			commonUtil.waitForElementToBeVisible(SF_FEED);
			commonUtil.clickButton(SF_FEED);
			test.log(LogStatus.INFO, "FEED in Sync Setting Salesforce Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Faild to Click on FEED in Sync Setting Salesforce");
			throw new AssertionError("Faild to Click on FEED in Sync Setting Salesforce", e);
		}

	}

	public void clickSave() {

		try {
			commonUtil.waitForElementToBeVisible(SF_SAVE_BTN);
			commonUtil.clickButton(SF_SAVE_BTN);
			test.log(LogStatus.INFO, "SAVE Button in Sync Setting Salesforce Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Faild to Click on SAVE Button in Sync Setting Salesforce");
			throw new AssertionError("Faild to Click on SAVE Button in Sync Setting Salesforce", e);
		}

	}
	public void clickSyncButton() {
		
		try {
			commonUtil.waitForElementToBeVisible(SF_Sync_BTN);
			commonUtil.clickButton(SF_Sync_BTN);
			test.log(LogStatus.INFO, "Sync Button of  Salesforce Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Faild to Click on Sync  Button in Salesforce");
			throw new AssertionError("Faild to Click on Sync Button in Salesforce", e);
		}
		
	}

	public void verifyChatCount() {

		try {
			commonUtil.waitForElementToBeVisible(CLOSE_CHAT);
			commonUtil.closechatIfExist(CLOSE_CHAT);
			test.log(LogStatus.INFO, "Chat window is already there TRUE");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Something went wrong");
		}
	}
	
	public void sFContactLName(String data) {

		try {
			String xpathforData = ".//a[contains(text(),'" + data + "')]";
			System.out.println("xpathforData: " + xpathforData);
			commonUtil.clickElement(xpathforData);
			test.log(LogStatus.INFO, data + "Field displayed and slected PASS");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, data + "Field displayed and slected FAIL");
			throw new AssertionError(data + "Field displayed and slected FAIL", e);
		}
		
	}
	
	/*public void checkSFCaseCheckBOX(int number) {
		
		Boolean checkStatus = commonUtil.verifyAngularElementForSalesforce("chat_type", "Name",number);
		if (checkStatus == true) {
			System.out.println("Already Case checked:");
		} else {
			commonUtil.click("salesFCase");
			commonUtil.selectFromDropdown("salesFCaseDrop", "Chat Transcript-#Name");
		}
	}
	
	public void checkSFNoteBOX() {
		
	}*/
}
