package com.acq.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.acq.qa.base.TestBase;
import com.acq.qa.util.Variables;
import com.relevantcodes.extentreports.LogStatus;

public class LoginPage extends TestBase {

	By SIGNIN_BUTTON = By.xpath(Variables.OR.getProperty("loginBtnLogin"));
	By USER_NAME = By.xpath(Variables.OR.getProperty("loginInEmail"));
	By PASSWORD = By.xpath(Variables.OR.getProperty("loginInPassword"));
	By DASHBOARD = By.xpath(Variables.OR.getProperty("dashboardDisplay"));
	By LINKFOR_LOGOUT = By.id("user-box-name-container");
	By LOGOUT_BUTTON = By.xpath(Variables.OR.getProperty("dashBtnLogout"));

	// Initializing the page objects
	public LoginPage() {

		PageFactory.initElements(driver, this);
	}

	// Actions
	public String validateLoginPageTitle() {
		System.out.println("login page" + SIGNIN_BUTTON);
		return driver.getTitle();
	}

	public void login(String userName, String password) {

		try {
			commonUtil.waitForElementToBeVisible(USER_NAME);
			commonUtil.sendKeys(USER_NAME, userName);
			commonUtil.waitForElementToBeVisible(PASSWORD);
			commonUtil.sendKeys(PASSWORD, password);
			commonUtil.clickButton(SIGNIN_BUTTON);
			test.log(LogStatus.INFO, "Login Successfully");

		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Login Failed ");
			throw new AssertionError("Login Failed", e);
		}
	}

	public String validateLogin() {

		System.out.println("login page" + SIGNIN_BUTTON);
		return driver.getTitle();
	}

	public void clickLinkforLogout() {

		try {
			commonUtil.waitForElementToBeVisible(LINKFOR_LOGOUT);
			commonUtil.clickButton(LINKFOR_LOGOUT);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Link for logout click Fail");
			throw new AssertionError("Link for logout click Fail", e);
		}
	}

	public void clickLogout() {

		try {
			commonUtil.waitForElementToBeVisible(LOGOUT_BUTTON);
			commonUtil.clickButton(LOGOUT_BUTTON);
			test.log(LogStatus.INFO, "Logout Successfully");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "logout button click Fail");
			throw new AssertionError("Logout button click Fail", e);
		}
	}
}
