package com.acq.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import com.acq.qa.base.TestBase;
import com.acq.qa.util.Variables;
import com.relevantcodes.extentreports.LogStatus;

public class CoBrowsePage extends TestBase {

	By COBROWSE_TAB = By.xpath(Variables.OR.getProperty("coBrTab"));
	By COBROWSE_BUTTON = By.xpath(Variables.OR.getProperty("coBrBtn"));
	By COBROWSEBACK_CHAT = By.xpath(Variables.OR.getProperty("dashInRightChatCobrowse"));


	public CoBrowsePage() {

		PageFactory.initElements(driver, this);
	}

	public void clickOnCobrowseTab() {

		try {
			commonUtil.clickButton(COBROWSE_TAB);
			test.log(LogStatus.INFO, "Co browse Tab clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Co browse Tab click Fail");
			throw new AssertionError("Co browse Tab click Fail", e);
		}
	}

	public void clickOnCobrowseButton() {

		try {
			commonUtil.clickButton(COBROWSE_BUTTON);
			test.log(LogStatus.INFO, "Co browse Button clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Co browse button click Fail");
			throw new AssertionError("Co browse button click Fail", e);
		}
	}

	public void enterTextBackendCoBrowse(String value) {

		try {
			commonUtil.sendKeysWithEnter(COBROWSEBACK_CHAT, value);
			test.log(LogStatus.INFO, value + " Text entered at Back end chat box for co browse ");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, value + " Back end chat box failed to enter Text for co browse");
			throw new AssertionError("Back end chat box failed to enter Text for co browse", e);
		}
	}

	

}
