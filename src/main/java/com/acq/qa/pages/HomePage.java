package com.acq.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import com.acq.qa.base.TestBase;
import com.acq.qa.util.Variables;
import com.relevantcodes.extentreports.LogStatus;

public class HomePage extends TestBase {
	
	//By Locators
	By USER_NAME = By.xpath(Variables.OR.getProperty("dashUsername"));
	By PERSONALTEST_BUTTON = By.xpath(Variables.OR.getProperty("dashBtnPersonalTestPage"));
	By FLOATING_CHAT_BTN = By.xpath(Variables.OR.getProperty("frontchatWidget"));
	By NEWCONV_BUTTON = By.xpath(Variables.OR.getProperty("frontNewconvBtn"));
	By INPUTTEXT_FRONTCHAT = By.xpath(Variables.OR.getProperty("frontTextarea"));
	By ANSWERCHAT_BUTTON = By.xpath(Variables.OR.getProperty("dashAnsChat"));
	By INPUTTEXT_BACKCHAT = By.xpath(Variables.OR.getProperty("dashInRightChat"));
	By CLOSEFRONT_CHAT = By.xpath(Variables.OR.getProperty("dashBtnCloseFront"));
	By CLOSEBACK_CHAT = By.xpath(Variables.OR.getProperty("dashBtnCloseBack"));
	By VIDEOCALL_BUTTON = By.xpath(Variables.OR.getProperty("dashVideoCallDisplay"));
	By NOTE_TAB = By.xpath(Variables.OR.getProperty("backendNotes"));
	By SAVENOTE_BUTTON = By.xpath(Variables.OR.getProperty("backendBtnSaveNote"));
	By PROFILE_TAB = By.xpath(Variables.OR.getProperty("backendTabProfile"));
	By SAVETOCRM_BUTTON = By.xpath(Variables.OR.getProperty("backendSavetoCRMBtn"));
	By VIDEOFRONT_SETTING_BUTTON = By.xpath(Variables.OR.getProperty("dashVideoSettingFront"));
	By VIDEOFRONT_RESIZE_BUTTON = By.xpath(Variables.OR.getProperty("dashVideoResizeBtnFront"));
	By VIDEOBACK_SETTING_BUTTON = By.xpath(Variables.OR.getProperty("dashVideoSettingBack"));
	By VIDEOBACK_RESIZE_BUTTON = By.xpath(Variables.OR.getProperty("dashVideoResizeBtnBack"));
	By CLOSE_POPUP_AGENT = By.xpath(Variables.OR.getProperty("dashBtnClosePopup"));
	By FILEUPLOAD_VISITOR = By.xpath(Variables.OR.getProperty("fileUploadVisitor"));
	By FILEDOWNLOADBTN_AGNT = By.xpath(Variables.OR.getProperty("filedownloadSymbol"));
	By CLOSE_CHAT = By.cssSelector(Variables.OR.getProperty("dashCloseChat"));
	By CLOSE_PENDINGCHAT = By.cssSelector(Variables.OR.getProperty("dashClosePendingChat"));
	By SELECT_RADIO = By.xpath(Variables.OR.getProperty("backendCountry"));
	By SELECT_CHECKBOXM = By.xpath(Variables.OR.getProperty("backendHobbyM"));
	By SELECT_CHECKBOXT = By.xpath(Variables.OR.getProperty("backendHobbyT"));
	By SELECT_CHECKBOXB = By.xpath(Variables.OR.getProperty("backendHobbyB"));
	By SELECT_DROPDOWN = By.xpath(Variables.OR.getProperty("backendDropdown"));
	By INPUTTEXT_BACKCHAT_ENTERTEXT = By.xpath(Variables.OR.getProperty("dashInRightChatEnter"));
	// By visitorName = By.cssSelector("(div > div:last-ch0ild > .name)[0]");

	//Page Actions
	public HomePage() {

		PageFactory.initElements(driver, this);
	}

	public String verifyHomepageTitle() {
		return driver.getTitle();
	}

	public boolean verifyUsername() {
		return driver.findElement(USER_NAME).isDisplayed();
	}

	public void clickOnPersonalTestPageBtn() {

		try {
			commonUtil.waitForElementToBeVisible(PERSONALTEST_BUTTON);
			commonUtil.clickButton(PERSONALTEST_BUTTON);
			test.log(LogStatus.INFO, "Personal test page button clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "personal test button click Fail");
			throw new AssertionError("Personal test button click Fail", e);
		}
	}

	public void clickFlotingchatBtn() {

		try {
			commonUtil.waitForElementToBeVisible(FLOATING_CHAT_BTN);
			commonUtil.clickButton(FLOATING_CHAT_BTN);
			test.log(LogStatus.INFO, "Floating chat button clicked");
			Thread.sleep(2000);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Floating chat button click Fail");
			throw new AssertionError("Floating chat test button click Fail", e);
		}
	}

	public void clickNewConvBtn() {

		try {
			commonUtil.waitForElementToBeVisible(NEWCONV_BUTTON);
			commonUtil.clickButton(NEWCONV_BUTTON);
			test.log(LogStatus.INFO, "New Coversation button Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "New Coversation button Clicked Fail");
			throw new AssertionError("New conversation button click Fail", e);
		}
	}

	public void clickAnsChatBtn() {

		try {
			Thread.sleep(3000);
			commonUtil.waitForElementToBeVisible(ANSWERCHAT_BUTTON);
			commonUtil.clickButton(ANSWERCHAT_BUTTON);
			test.log(LogStatus.INFO, "Answer chat button clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Answer Chat button Clicked Fail ");
			throw new AssertionError("Answer chat button clicked Fail", e);
		}
	}

	public void enterTextFrontend(String value) {

		try {
			commonUtil.waitForElementToBeVisible(INPUTTEXT_FRONTCHAT);
			commonUtil.sendKeys(INPUTTEXT_FRONTCHAT, value);
			driver.findElement(INPUTTEXT_FRONTCHAT).sendKeys(Keys.ENTER);
			test.log(LogStatus.INFO, "Text entered at Front end chat box :: " + value);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Front end chat box failed to enter Text :: " + value);
			throw new AssertionError("Front end chat box failed to enter Text", e);
		}
	}
	
	
	public void enterTextBackend(String value) {

		try {
			commonUtil.waitForElementToBeVisible(INPUTTEXT_BACKCHAT);
			commonUtil.sendKeys(INPUTTEXT_BACKCHAT, value);
			driver.findElement(INPUTTEXT_BACKCHAT_ENTERTEXT).sendKeys(Keys.ENTER);
			test.log(LogStatus.INFO, "Text entered at Back end chat box ::  " + value);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Back end chat box failed to enter Text :: " + value);
			throw new AssertionError("Back end chat box failed to enter Text", e);
		}
	}

	public void closeChatFrontend() {

		try {
			commonUtil.waitForElementToBeVisible(CLOSEFRONT_CHAT);
			commonUtil.clickButton(CLOSEFRONT_CHAT);
			test.log(LogStatus.INFO, "Front end close chat button clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Front end close chat button clicked Fail");
			throw new AssertionError("Front end close chat button click to Fail", e);
		}
	}

	public void closeChatBackend() {

		try {
			commonUtil.waitForElementToBeVisible(CLOSEBACK_CHAT);
			commonUtil.clickButton(CLOSEBACK_CHAT);
			test.log(LogStatus.INFO, "Back end close chat button clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Back end close chat button click to Fail");
			throw new AssertionError("Back end close chat button click to Fail", e);
		}
	}

	public void videoChatBtnDisplay() {

		try {
			Thread.sleep(2000);
			commonUtil.waitForElementToBeVisible(VIDEOCALL_BUTTON);
			test.log(LogStatus.INFO, "videocall button displayed");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "videocall button not visible");
			throw new AssertionError("videocall button not visible", e);
		}
	}

	public void clickNoteTab() {

		try {
			commonUtil.waitForElementToBeVisible(NOTE_TAB);
			commonUtil.clickButton(NOTE_TAB);
			test.log(LogStatus.INFO, "Notes Tab Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Notes Tab click to Fail");
			throw new AssertionError("Notes Tab click to Fail", e);
		}
	}

	public void saveNoteButton() {

		try {
			commonUtil.waitForElementToBeVisible(SAVENOTE_BUTTON);
			commonUtil.clickButton(SAVENOTE_BUTTON);
			test.log(LogStatus.INFO, "Save Note Button Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Save Note Button click to Fail");
			throw new AssertionError("Save Note Button click to Fail", e);
		}
	}

	public void clickProfileTab() {

		try {
			commonUtil.waitForElementToBeVisible(PROFILE_TAB);
			commonUtil.clickButton(PROFILE_TAB);
			test.log(LogStatus.INFO, "Profile Tab Clicked");
			System.out.println("Profile TAB Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Profile Tab click to Fail");
			throw new AssertionError("Profile Tab click to Fail", e);
		}
	}

	public void saveToCRMButton() {

		try {
			commonUtil.waitForElementToBeVisible(SAVETOCRM_BUTTON);
			commonUtil.clickButton(SAVETOCRM_BUTTON);
			test.log(LogStatus.INFO, "Save to CRM Button Clicked");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Save to CRM Button click to Fail");
			throw new AssertionError("Save to CRM Button click to Fail", e);
		}

	}

	public void verifySettingButtonFrontend() {

		try {
			commonUtil.waitForElementToBeVisible(VIDEOFRONT_SETTING_BUTTON);
			test.log(LogStatus.INFO, "Setting button is displayed true for co browse video call at Frontend");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, " Setting button is not displayed true for video call at Frontend");
			throw new AssertionError("Setting button is not displayed true for video call at Frontend", e);
		}
	}

	public void verifyResizeButtonFrontend() {

		try {
			commonUtil.waitForElementToBeVisible(VIDEOFRONT_RESIZE_BUTTON);
			test.log(LogStatus.INFO, "Resize button is displayed true for video call at Frontend");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, " Resize button is not displayed true for video call at Frontend");
			throw new AssertionError("Resize button is not displayed true for video call at Frontend", e);
		}
	}

	public void verifySettingButtonBackend() {

		try {
			commonUtil.waitForElementToBeVisible(VIDEOBACK_SETTING_BUTTON);
			test.log(LogStatus.INFO, "Setting button is displayed true for video call at Backend");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, " Setting button is not displayed true for video call at Backend");
			throw new AssertionError("Setting button is not displayed true for video call at Backend", e);
		}
	}

	public void verifyResizeButtonBackend() {

		try {
			commonUtil.waitForElementToBeVisible(VIDEOBACK_RESIZE_BUTTON);
			test.log(LogStatus.INFO, "Resize button is displayed true for video call at Backend");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, " Resize button is not displayed true for video call at Backend");
			throw new AssertionError("Resize button is not displayed true for video call at Backend", e);
		}
	}

	public void verifyFileUploadBtnVisitor() {

		try {
			commonUtil.waitForElementToBeVisible(FILEUPLOAD_VISITOR);
			test.log(LogStatus.INFO, "Fileupload Button is displayed TRUE");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Fileupload Button is displayed FALSE");

		}
	}

	public void verifyFileDonloadBtnDisplay() {

		try {
			commonUtil.waitForElementToBeVisible(FILEDOWNLOADBTN_AGNT);
			test.log(LogStatus.INFO, "FileDownload Button is displayed TRUE");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "FileDownload Button is displayed FALSE");
		}
	}

	public void verifyChatCount() {

		try {
			commonUtil.waitForElementToBeVisible(CLOSE_CHAT);
			commonUtil.closechatIfExist(CLOSE_CHAT);
			test.log(LogStatus.INFO, "Chat window is already there TRUE");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Something went wrong");
		}
	}

	public void verifyPnedingchat() {

		try {
			commonUtil.waitForElementToBeVisible(CLOSE_PENDINGCHAT);
			commonUtil.closePendingChat(CLOSE_PENDINGCHAT, CLOSE_CHAT);
			test.log(LogStatus.INFO, "Pending Chat window is already there TRUE");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Something went wrong");
		}
	}

	public void selectFromRadioButton() {

		try {
			Thread.sleep(1000);
			commonUtil.selectRadioOrCheckbox(SELECT_RADIO);
			test.log(LogStatus.INFO, "Radio button Selected");
		} catch (Exception e) {
			test.log(LogStatus.INFO, "Radio button is not Selected");
			throw new AssertionError("Radio button is not Selected", e);
		}

	}

	public void seletHobby1() {

		try {
			Thread.sleep(1000);
			commonUtil.selectRadioOrCheckbox(SELECT_CHECKBOXT);
			test.log(LogStatus.INFO, "Travelling Checkbox Selected");
		} catch (Exception e) {
			test.log(LogStatus.INFO, "Travelling Checkbox is not Selected");
			throw new AssertionError("Travelling Checkbox is not Selected", e);
		}
	}

	public void seletHobby2() {

		try {
			Thread.sleep(1000);
			commonUtil.selectRadioOrCheckbox(SELECT_CHECKBOXM);
			test.log(LogStatus.INFO, "Movie Checkbox Selected");
		} catch (Exception e) {
			test.log(LogStatus.INFO, "Movie Checkbox is not Selected");
			throw new AssertionError("Movie Checkbox is not Selected", e);
		}
	}

	public void seletHobby3() {

		try {
			Thread.sleep(1000);
			commonUtil.selectRadioOrCheckbox(SELECT_CHECKBOXB);
			test.log(LogStatus.INFO, "Bungee jumping Checkbox Selected");
		} catch (Exception e) {
			test.log(LogStatus.INFO, "Bungee jumping Checkbox is not Selected");
			throw new AssertionError("Bungee jumping Checkbox is not Selected", e);
		}
	}

}
