package com.acq.qa.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.acq.integration.pages.SalesforceContacts;
import com.acq.qa.pages.CoBrowsePage;
import com.acq.qa.pages.HomePage;
import com.acq.qa.pages.LoginPage;
import com.acq.qa.util.CommonUtils;
import com.acq.qa.util.ExtentManager;
import com.acq.qa.util.ReadJson;
import com.acq.qa.util.UserData;
import com.acq.qa.util.Variables;
import com.github.javafaker.Faker;
import com.paulhammant.ngwebdriver.NgWebDriver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestBase {

	public static WebDriver driver;
	public static NgWebDriver ngdriver;
	public static Properties prop;
	public static FileInputStream fis, fis1;
	public static FileInputStream jsonFile;
	public static EventFiringWebDriver e_driver;
	public static Logger log = Logger.getLogger("devpinoyLogger");
	public ExtentReports rep = ExtentManager.getInstance();
	public static ExtentTest test;
	public static CommonUtils commonUtil = new CommonUtils();
	public static HomePage homePage = new HomePage();
	public static UserData uData = new UserData();
	public static LoginPage loginPage = new LoginPage();
	public static SalesforceContacts salesforce = new SalesforceContacts();
	public static CoBrowsePage cobrowse = new CoBrowsePage();
	public static Faker faker = new Faker();

	public TestBase() {

		Variables.OR = new Properties();
		Variables.CR = new Properties();
		
		try {
			fis = new FileInputStream(
					System.getProperty("user.dir") + "/src/main/java" + "/objectRepository/OR.properties");
			fis1 = new FileInputStream(
					System.getProperty("user.dir") + "/src/main/java" + "/objectRepository/Credential.properties");
			log.info("Properties file found");
		} catch (FileNotFoundException e) {
			log.error("Properties file not found");
			e.printStackTrace();
		}

		try {
			Variables.OR.load(fis);
			Variables.CR.load(fis1);
			log.info("Properties file loaded !!!");
		} catch (IOException e) {
			log.error("Properties file not loaded");
			e.printStackTrace();
		}
		
		ReadJson jsonread = new ReadJson();
	
		try {
			jsonread.getJsonData();
			log.info("JSON file loaded !!!");
		} catch (Exception e) {
			log.error("JSON file not loaded");
			e.printStackTrace();
		}
	}

	public static void initialization() {

		if (driver == null) {
			UserData uData = new UserData();
			String browserName = uData.getBrowser();
			
			if (browserName.equals("chrome")) {
				
				Map<String, Object> prefs = new HashMap<String, Object>();
				WebDriverManager.chromedriver().setup();
				prefs.put("profile.default_content_setting_values.notifications", 2);
				prefs.put("credentials_enable_service", false);
				prefs.put("profile.password_manager_enabled", false);
				ChromeOptions options = new ChromeOptions();
				//options.addArguments("--headless");
				options.setPageLoadStrategy(PageLoadStrategy.NONE);
				options.setExperimentalOption("prefs", prefs);
				options.addArguments("--disable-extensions");
				options.addArguments("--incognito");
				options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
				options.setExperimentalOption("useAutomationExtension", false);
				options.addArguments("use-fake-ui-for-media-stream");
				driver = new ChromeDriver(options);
				log.info("Chrome Browser launched");

			} else if (browserName.equals("firefox")) {
				
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("permissions.default.microphone", 1);
				prefs.put("permissions.default.camera", 1);
				prefs.put("credentials_enable_service", false);
				prefs.put("profile.password_manager_enabled", false);
				WebDriverManager.firefoxdriver().setup();
				FirefoxOptions options = new FirefoxOptions();
				options.addPreference("dom.webnotifications.enabled", false);
				options.addPreference("permissions.default.microphone", 1);
				options.addPreference("permissions.default.camera", 1);
				driver = new FirefoxDriver(options);

			}

			else if (browserName.equals("edge")) {
				
				WebDriverManager.edgedriver().setup();
				EdgeOptions options = new EdgeOptions();
				options.setCapability("dom.webnotifications.enabled", 1);
				options.setCapability("permissions.default.microphone", 1);
				options.setCapability("permissions.default.camera", 1);
				driver = new EdgeDriver(options);
				ngdriver = new NgWebDriver((JavascriptExecutor) driver);
			}

			else if (browserName.equals("ie")) {

				WebDriverManager.iedriver().setup();
				InternetExplorerOptions options = new InternetExplorerOptions();
				options.setCapability("dom.webnotifications.enabled", 1);
				options.setCapability("permissions.default.microphone", 1);
				options.setCapability("permissions.default.camera", 1);
				driver = new InternetExplorerDriver(options);
				ngdriver = new NgWebDriver((JavascriptExecutor) driver);
			}

			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().pageLoadTimeout(CommonUtils.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(CommonUtils.IMPLICIT_WAIT, TimeUnit.SECONDS);
		}
	}
}
