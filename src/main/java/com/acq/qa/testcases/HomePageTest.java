package com.acq.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.acq.qa.base.TestBase;
import com.acq.qa.util.CommonUtils;
import com.acq.qa.util.Variables;

public class HomePageTest extends TestBase {
	
	public HomePageTest() {
		super();
	}

	@BeforeMethod
	public void setUp() throws Exception {
		initialization();
		CommonUtils.enviroment(uData.getEnviroment());
	}

	@Test(priority = 1,enabled = true)
	public void chatTest() throws Exception {

		commonUtil.loginToChatProcess();
		commonUtil.verifyChatBackend("hello how are you");
		homePage.enterTextBackend("Tomorrowland is an electronic dance music festival");
		Thread.sleep(2000);
		commonUtil.switchTab("1");
		commonUtil.switchframeByXpath("frontchatIframe");
		commonUtil.verifyChatFrontend("Tomorrowland is an electronic dance music festival");
		commonUtil.logoutProcess();
	}

	@Test(priority = 2,enabled = true)
	public void videoCallTest() throws Exception {
		
		commonUtil.loginToChatProcess();
		homePage.videoChatBtnDisplay(); 
		commonUtil.click("dashVideoCallDisplay");
		commonUtil.switchTab("1");
		Thread.sleep(2000);
		commonUtil.switchframeByXpath("frontchatIframe");
		Thread.sleep(1000);
		commonUtil.click("dashAnsasVideocall");
		commonUtil.switchTab("0");
		Thread.sleep(2000);
		homePage.verifySettingButtonBackend();
		homePage.verifyResizeButtonBackend();
		Thread.sleep(5000);
		commonUtil.click("dashcallcanelBack");
		commonUtil.switchTab("1");
		commonUtil.switchframeByXpath("frontchatIframe");
		commonUtil.logoutProcess();
	}

	@Test(priority = 3,enabled = false)
	public void saveNoteTest() throws Exception {
		
		commonUtil.loginToChatProcess();
		homePage.clickNoteTab();
		commonUtil.verifyText("backendTitle", "Title");
		commonUtil.inputTextEnter("backendInTitle", "Test");
		commonUtil.inputTextEnter("backendInNoteMessage", "Automation");
		homePage.saveNoteButton();
		commonUtil.verifyText("backendVerifyTitle", "Test");
		commonUtil.verifyText("backendVerifyMsg", "Automation");
		commonUtil.switchTab("1");
		commonUtil.switchframeByXpath("frontchatIframe");
		commonUtil.logoutProcess();
	}

	@Test(priority = 4,enabled = false)
	public void saveProfileTest() throws Exception {
		
		commonUtil.loginToChatProcess();
		homePage.clickProfileTab();
		commonUtil.inputText("backendInName", "Jagrut");
		commonUtil.inputText("backendInEmail", "test@gmail.com");
		commonUtil.inputText("backendInPhone", "8787989878");
		commonUtil.inputText("backendInRemarks", "Test Remarks");
		commonUtil.scrollTillElement("backendSavetoCRMBtn");
		homePage.saveToCRMButton();
		commonUtil.verifyAngularElement("UserNick", "Jagrut","Name");
		commonUtil.verifyAngularElement("Email", "test@gmail.com","Name");
		commonUtil.verifyAngularElement("UserPhone", "8787989878","Name");
		commonUtil.verifyAngularElement("textarea", "Test Remarks","TagName");
		commonUtil.switchTab("1");
		commonUtil.switchframeByXpath("dashFirstFendChatIframe");
		commonUtil.logoutProcess();
		
	}

	@Test(priority = 5,enabled = false)
	public void fileUploadTest() throws Exception {
		
		commonUtil.loginToChatProcess();
		commonUtil.switchTab("1");
		commonUtil.switchframeByXpath("frontchatIframe");
		homePage.verifyFileUploadBtnVisitor();
		commonUtil.click("fileUploadVisitor");
		commonUtil.uploadFileWithRobot(Variables.attachmentImagePath);
		commonUtil.switchTab("0");
		homePage.verifyFileDonloadBtnDisplay();
		commonUtil.click("filedownloadSymbol");
		commonUtil.switchTab("2");
		commonUtil.readPDFInURL("Jagrut Solanki");
		commonUtil.switchTab("1");
		commonUtil.logoutProcess();	
	}
	
	@AfterMethod
	public void tearDown() throws Exception {

		driver.quit();
		driver = null;
	}
}
