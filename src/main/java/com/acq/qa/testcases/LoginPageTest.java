package com.acq.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.acq.qa.base.TestBase;
import com.acq.qa.util.CommonUtils;

public class LoginPageTest extends TestBase {

	public LoginPageTest() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
		CommonUtils.enviroment(uData.getEnviroment());
	}

	@Test(priority = 1)
	public void loginTest() {

		String title = loginPage.validateLoginPageTitle();
		Assert.assertEquals(title, "Login");
		loginPage.login(commonUtil.userName(uData.getUserEmail()), commonUtil.userPassword(uData.getUserPassword()));
	}

	@Test(priority = 2)
	public void logoutTest() throws Exception {

		loginTest();
		loginPage.clickLinkforLogout();
		loginPage.clickLogout();
	}

	@AfterMethod
	public void tearDown() {

		driver.quit();
		driver = null;
	}
}
