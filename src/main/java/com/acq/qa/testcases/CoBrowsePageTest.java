package com.acq.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.acq.qa.base.TestBase;
import com.acq.qa.util.CommonUtils;

public class CoBrowsePageTest extends TestBase {

	public CoBrowsePageTest() {

		super();
	}

	@BeforeMethod
	public void setUp() throws Exception {

		initialization();
		CommonUtils.enviroment(uData.getEnviroment());
	}

	@Test(priority = 1,enabled = true)
	public void CobrowseChatTest() throws Exception {
		
		commonUtil.loginToChatProcess();
		cobrowse.clickOnCobrowseTab();
		cobrowse.clickOnCobrowseButton();
		commonUtil.switchTab("1");
		commonUtil.switchframeByXpath("dashClosePopupIframe");	
		commonUtil.click("dashBtnClosePopup");
		commonUtil.switchframeByXpath("coBroFrontFrame");
		commonUtil.verifyText("coBrowseTest", "The Co-Browse Test");
		commonUtil.switchToMainFrame();
		commonUtil.switchframeByXpath("frontChatWidgetIframe");
		homePage.clickFlotingchatBtn();
		commonUtil.switchToMainFrame();
		commonUtil.switchframeByXpath("frontchatIframe");
		homePage.enterTextFrontend("Hello cobrowse from frontend");
		commonUtil.switchTab("0");
		commonUtil.switchframeByXpath("coBroBackFrame");
		commonUtil.verifyText("coBrowseTest", "The Co-Browse Test");
		commonUtil.switchToMainFrame();
		commonUtil.verifyChatBackend("Hello cobrowse from frontend");
		cobrowse.enterTextBackendCoBrowse("hello from backend cobrowse");
		commonUtil.switchTab("1");
		commonUtil.switchframeByXpath("frontchatIframe");
		commonUtil.verifyChatFrontend("hello from backend cobrowse");
		/*commonUtil.switchTab("0");
		commonUtil.inputTextEnter("coBroURL", "https://www.google.com");
		commonUtil.switchTab("1");
		commonUtil.switchframeByXpath("frontCobrowseMainIframe");
		commonUtil.waitforelement("coBroGoogleLinkAbout", 20);
		commonUtil.verifyText("coBroGoogleLinkAbout", "About");
		commonUtil.waitforelement("coBroGoogleLinkAbout", 20);
		commonUtil.verifyText("CoBroGoogleLinkStore", "Store");*/
		commonUtil.switchToMainFrame();
		commonUtil.switchframeByXpath("dashFirstFendChatIframe");
		commonUtil.logoutProcess();
	}

	@Test(priority = 2,enabled = true)
	public void coBrowseVideoTest() throws Exception {

		commonUtil.loginToChatProcess();
		cobrowse.clickOnCobrowseTab();
		cobrowse.clickOnCobrowseButton();
		commonUtil.switchTab("1");
		commonUtil.switchframeByXpath("dashClosePopupIframe");	
		commonUtil.click("dashBtnClosePopup");
		commonUtil.switchTab("0");
		homePage.videoChatBtnDisplay();
		commonUtil.click("dashVideoCallDisplay");
		commonUtil.switchTab("1");
		commonUtil.switchframeByXpath("frontchatIframe");
		commonUtil.click("dashAnsasVideocall");
		//homePage.verifySettingButtonFrontend();
		//homePage.verifyResizeButtonFrontend();
		commonUtil.switchTab("0");
		Thread.sleep(5000);
		homePage.verifySettingButtonBackend();
		homePage.verifyResizeButtonBackend();
		commonUtil.switchTab("1");
		commonUtil.switchframeByXpath("frontchatIframe");
		commonUtil.logoutProcess();
	}

	@AfterMethod
	public void tearDown() throws Exception {

		driver.quit();
		driver = null;

	}

}
