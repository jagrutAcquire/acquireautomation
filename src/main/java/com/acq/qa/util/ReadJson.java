package com.acq.qa.util;

import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ReadJson {

	public void getJsonData() throws Exception {

		FileInputStream inputStream = new FileInputStream(
				System.getProperty("user.dir") + "/src/main/java" + "/objectRepository/Config.json");
		JSONParser parser = new JSONParser();
		JSONObject jsonobj = (JSONObject) parser.parse(new InputStreamReader(inputStream));
		UserData uData = new UserData();
		uData.setBrowser((String) jsonobj.get("browser"));
		uData.setEnviroment((String) jsonobj.get("enviroment"));
		uData.setProdUsername((String) jsonobj.get("prodUsername"));
		uData.setProdPassword((String) jsonobj.get("prodPassword"));
		uData.setStageUsername((String) jsonobj.get("stageUsername"));
		uData.setStagePassword((String) jsonobj.get("stagePassword"));
		uData.setStageUrl((String) jsonobj.get("STAGE"));
		uData.setProdUrl((String) jsonobj.get("PROD"));
		uData.setUserEmail((String) jsonobj.get("userEmail"));
		uData.setUserPassword((String) jsonobj.get("userPassword"));
		uData.setLiveUserUrl((String) jsonobj.get("liveUserUrl"));
		uData.setStageUserURL((String) jsonobj.get("stageUserURL"));
		uData.setUserUrl((String) jsonobj.get("userUrl"));
		uData.setSalesfroceURL((String) jsonobj.get("SalesforceURL"));
	}
}
