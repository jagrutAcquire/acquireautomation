package com.acq.qa.util;

public class UserData {

	private static String browser;
	private static String uatUrl;
	private static String prodUrl;
	private static String stageUrl;
	private static String devUrl;
	private static String userName;
	private static String userEmail;
	private static String userPassword;
	private static String liveUserUrl;
	private static String enviroment;
	private static String stageUserURL;
	private static String userUrl;
	private static String salesfroceURL;
	private static String prodUsername;
	private static String prodPassword;
	private static String stageUsername;
	private static String stagePassword;

	public String getLiveUserUrl() {
		return liveUserUrl;
	}

	public void setLiveUserUrl(String liveUserUrl) {
		UserData.liveUserUrl = liveUserUrl;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		UserData.browser = browser;
	}

	public String getUatUrl() {
		return uatUrl;
	}

	public void setUatUrl(String uatUrl) {
		UserData.uatUrl = uatUrl;
	}

	public String getProdUrl() {
		return prodUrl;
	}

	public void setProdUrl(String prodUrl) {
		UserData.prodUrl = prodUrl;
	}

	public String getStageUrl() {
		return stageUrl;
	}

	public void setStageUrl(String stageUrl) {
		UserData.stageUrl = stageUrl;
	}

	public String getDevUrl() {
		return devUrl;
	}

	public void setDevUrl(String devUrl) {
		UserData.devUrl = devUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		UserData.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		UserData.userEmail = userEmail;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		UserData.userPassword = userPassword;
	}

	public String getEnviroment() {
		return enviroment;
	}

	public void setEnviroment(String enviroment) {
		UserData.enviroment = enviroment;
	}

	public String getStageUserURL() {
		return stageUserURL;
	}

	public void setStageUserURL(String stageUserURL) {
		UserData.stageUserURL = stageUserURL;
	}

	public String getUserUrl() {
		return userUrl;
	}

	public void setUserUrl(String userUrl) {
		UserData.userUrl = userUrl;
	}

	public String getSalesfroceURL() {
		return salesfroceURL;
	}

	public void setSalesfroceURL(String salesfroceURL) {
		UserData.salesfroceURL = salesfroceURL;
	}

	public String getProdUsername() {
		return prodUsername;
	}

	public void setProdUsername(String prodUsername) {
		UserData.prodUsername = prodUsername;
	}

	public String getProdPassword() {
		return prodPassword;
	}

	public void setProdPassword(String prodPassword) {
		UserData.prodPassword = prodPassword;
	}

	public String getStageUsername() {
		return stageUsername;
	}

	public void setStageUsername(String stageUsername) {
		UserData.stageUsername = stageUsername;
	}

	public String getStagePassword() {
		return stagePassword;
	}

	public void setStagePassword(String stagePassword) {
		UserData.stagePassword = stagePassword;
	}
	
	
}
