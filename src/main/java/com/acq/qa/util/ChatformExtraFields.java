package com.acq.qa.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.acq.qa.base.TestBase;
import com.acq.qa.pages.LoginPage;

public class ChatformExtraFields extends TestBase {
	public static LoginPage loginPage;
	public static WebDriverWait wait;

	public static void scrollTillElement(String xpath) throws Exception {

		Point hoverItem = driver.findElement(By.xpath(xpath)).getLocation();
		((JavascriptExecutor) driver).executeScript("return window.title;");
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0," + (hoverItem.getY() - 400) + ");");
	}

	public static void waitForElementToBeVisible(By selector) {

		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		} catch (Exception e) {
			throw new NoSuchElementException(String.format("Element was not visible:: ", selector));
		}
	}

	public static void click(String xpath) {

		try {
			driver.findElement(By.xpath((xpath))).click();
			System.out.println(xpath + "Button clicked");
			Thread.sleep(800);
		} catch (Exception e) {
			System.err.println("Cannot Click " + e.getMessage());
			throw new AssertionError("Unable to click on ::  " + xpath + " Button", e);
		}
	}

	public static List<String> extractTagValues() throws IOException {

		String fileName = "C://JAVA For Exam/SugarCRM-Lead.txt";
		List<String> tagItems = new ArrayList<String>();
		File file = new File(fileName);

		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		System.out.println("Reading text file using FileReader");
		while ((line = br.readLine()) != null) {
			Pattern pattern = Pattern.compile("<option.*>(.+?)</option>");
			Matcher matcher = pattern.matcher(line);

			while (matcher.find()) {
				String anchorText = matcher.group(1);
				tagItems.add(anchorText.trim());
				System.out.println("Fields = " + tagItems);
			}
		}
		br.close();
		fr.close();
		return tagItems;
	}

	public static void createCustomField() throws Exception {

		extractTagValues();
		initialization();
		driver.findElement(By.id("loginform-email")).sendKeys("jagrutsolankitest@gmail.com");
		Thread.sleep(1000);
		driver.findElement(By.id("loginform-password")).sendKeys("jagrutsolankitest@gmail.com");
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//button[contains(text(),'Log in')]")).click();
		Thread.sleep(3000);
		driver.get("https://app.acquire.io/widget/form");
		Thread.sleep(1000);
		scrollTillElement(".//p[@id='addonlinefield']");
		Boolean isPresent = driver.findElements(By.xpath(".//a[@class='del-button btn icon-cancel delete-confirm']"))
				.size() > 0;
		System.out.println("status" + isPresent);
		if (isPresent.equals(true)) {
			System.out.println("Element is Visible");
			scrollTillElement(".//a[@class='del-button btn icon-cancel delete-confirm']");
			List<WebElement> allElements = driver
					.findElements(By.xpath(".//a[@class='del-button btn icon-cancel delete-confirm']"));
			int s = allElements.size();

			System.out.println("Total Element for Remove is: " + s);

			for (int i = 0; i < s; i++) {
				Thread.sleep(1000);
				allElements.get(i).click();
				Thread.sleep(1000);
				WebElement popup = driver.findElement(By.xpath(".//button[contains(text(),'Yes')]"));
				popup.click();
				Thread.sleep(1000);
			}
		} else {
			System.out.println("Element is InVisible");
		}
	}

	public static void editCustomField() throws Exception {

		List<String> tagItems = extractTagValues();
		for (int i = 1; i < tagItems.size(); i++) {
			Thread.sleep(1000);
			driver.findElement(By.id("addonlinefield")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath(
					".//li[@class='icon-text input-control input-control-9']//preceding::li[@class='icon-text input-control input-control-9']"))
					.click();
			Thread.sleep(1000);
			List<WebElement> allElement = driver.findElements(By.xpath(".//a[@title='Edit']"));
			List<WebElement> allNameElement = driver.findElements(By.xpath(".//input[@name='name']"));
			List<WebElement> allLabelElement = driver.findElements(By.xpath(".//input[@name='label']"));
			int count = allElement.size();
			int count1 = allLabelElement.size();
			int count2 = allNameElement.size();

			scrollTillElement(".//a[@class='del-button btn icon-cancel delete-confirm']");
			allElement.get(count - 1).click();
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(allLabelElement.get(count1 - 1))).clear();
			// allLabelElement.get(count1 - 1).clear();
			allLabelElement.get(count1 - 1).sendKeys(tagItems.get(i));
			Thread.sleep(1000);
			allNameElement.get(count2 - 1).clear();
			allNameElement.get(count2 - 1).sendKeys(tagItems.get(i));
			driver.findElement(By.xpath(".//a[@class='toggle-form btn icon-pencil open']")).click();
			Thread.sleep(1000);
		}
		driver.findElement(By.xpath(".//button[@id='onGetData']")).click();
	}

	public static void main(String args[]) throws Exception {

		createCustomField();
		editCustomField();
		scrollTillElement(".//div[@id='user-box-name-container']");
		waitForElementToBeVisible(By.id("user-box-name-container"));
		click(".//div[@id='user-box-name-container']");
		waitForElementToBeVisible(By.xpath("//a[contains(text(),'Log out')]"));
		click("//a[contains(text(),'Log out')]");
		driver.quit();
		driver = null;
	}
}
