package com.acq.qa.util;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.EmptyFileException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.TestException;

import com.acq.qa.base.TestBase;
import com.github.javafaker.Faker;
import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import com.google.common.collect.Iterables;
import com.relevantcodes.extentreports.LogStatus;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CommonUtils extends TestBase {

	public static long PAGE_LOAD_TIMEOUT = 50;
	public static long IMPLICIT_WAIT = 50;
	private static int timeout = 100;
	public static WebElement data;
	public String screenshotPath;
	public String screenshotName;
	public static WebDriverWait wait;
	public String text;
	public static HashMap<Integer, List<String>> Global_totalWindow = new HashMap<Integer, List<String>>();
	public static int Global_countForWindow = 0;
	static Faker faker = new Faker();

	
	public void switchframeByXpath(String xpath) throws Exception {

		try {
			System.out.println("switch to iframe");
			waitforelement(xpath, 30);
			WebDriver x = driver.switchTo().frame(driver.findElement(By.xpath(Variables.OR.getProperty(xpath))));
			// driver.switchTo().defaultContent();
			test.log(LogStatus.INFO, "iFrame Switched to " + x.getTitle());
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Can not Switch Iframe");
			throw new AssertionError("Can not Switch Iframe", e);
		}
	}

	public void captureScreenshot() throws IOException {
		try {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			Date d = new Date();
			screenshotName = d.toString().replace(":", "_").replace(" ", "_") + ".jpg";
			FileUtils.copyFile(scrFile, new File(
					System.getProperty("user.dir") + "\\target\\surefire-reports\\AcquireTest\\" + screenshotName));
		} catch (Exception e) {
			System.err.println("Screenshot capture Failed");
		}
	}

	public void switchTab(String tabNumber) {

		try {
			Thread.sleep(800);
			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			int tab = Integer.parseInt(tabNumber);
			driver.switchTo().window(tabs2.get(tab));
			test.log(LogStatus.INFO, "Switch tab to :: " + tab);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Can not Switch Tab :: " + e.getMessage());
			throw new AssertionError("Can not Switch Tab", e);
		}
	}

	public static void enviroment(String env) {

		try {
			System.out.println("Enters into Enviroment : " + env);
			UserData uData = new UserData();
			System.out.println("Enviroment is " + env);
			switch (env) {
			case "UAT":
				driver.get(uData.getUatUrl());
				log.info("Navigated to " + uData.getUatUrl());
				break;
			case "STAGE":
				driver.get(uData.getStageUrl());
				log.info("Navigated to " + uData.getStageUrl());
				break;
			case "DEV":
				driver.get(uData.getDevUrl());
				log.info("Navigated to " + uData.getDevUrl());
				break;
			case "PROD":
				driver.get(uData.getProdUrl());
				waitForPageLoad();
				log.info("Navigated to " + uData.getProdUrl());
				break;
			default:
				driver.get(uData.getUatUrl());
				log.info("Navigated to " + uData.getUatUrl());
				break;
			}
		} catch (Exception e) {
			System.out.println("FAILURE: URL did not load: " + env);
			test.log(LogStatus.ERROR, "URL did not load because " + e.getMessage());
			throw new AssertionError("URL did not load  ", e);
		}
	}
	public static void openNewWindow(String env) {

		try {
			System.out.println("Enters into User Page URL Enviroment : " + env);
			UserData uData = new UserData();
			System.out.println("Enviroment is " + env);
			// Initialize the robot class object
			Robot robot = new Robot();

			// Press and hold Control and N keys
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_N);

			// Release Control and N keys
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_N);

			// Set focus to the newly opened browser window
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(tabs.size() - 1));
			for (String windowHandle : driver.getWindowHandles()) {
				driver.switchTo().window(windowHandle);
			}
			switch (env) {
			case "liveUserUrl":
				driver.get(uData.getLiveUserUrl());
				log.info("Navigated to " + uData.getLiveUserUrl());
				break;
			case "stageUserURL":
				driver.get(uData.getStageUserURL());
				log.info("Navigated to " + uData.getStageUserURL());
				break;
			default:
				driver.get(uData.getLiveUserUrl());
				log.info("Navigated to " + uData.getLiveUserUrl());
				break;
			}
		} catch (Exception e) {
			System.out.println("FAILURE: User URL did not load: " + env);
			test.log(LogStatus.ERROR, "User URL did not load because " + e.getMessage());
			throw new AssertionError("Usser URL did not load  ", e);
		}
	}
	
	
	public String userName(String env) {
		String username;
		try {
			System.out.println("Enters into Check Username : " + env);
			UserData uData = new UserData();
			System.out.println("Username is " + env);
			switch (env) {
			case "prodUsername":
				username = uData.getProdUsername();
				System.out.println("Username: "+username);
				break;
			case "stageUsername":
				username = uData.getStageUsername();
				System.out.println("Username: "+username);
				break;
			default:
				username = uData.getProdUsername();
				System.out.println("Username: "+username);
				log.info("Username is" + uData.getProdUsername());
				break;
			}
		} catch (Exception e) {
			System.out.println("FAILURE: Username failed to load: " + env);
			test.log(LogStatus.ERROR, "Username did not load because " + e.getMessage());
			throw new AssertionError("Username did not load  ", e);
		}
		return username;
	}
	
	public String userPassword(String env) {
		String password = "";
		try {
			System.out.println("Enters into Check User Password : " + env);
			UserData uData = new UserData();
			System.out.println("Password is " + env);
			switch (env) {
			case "prodPassword":
				password = uData.getProdPassword();
				System.out.println("password: "+password);
				break;
			case "stagePassword":
				password = uData.getStagePassword();
				System.out.println("password: "+password);
				break;
			default:
				driver.get(uData.getUatUrl());
				log.info("Password is " + uData.getProdPassword());
				break;
			}
		} catch (Exception e) {
			System.out.println("FAILURE: User Password failed to load: " + env);
			test.log(LogStatus.ERROR, "User Password did not load because " + e.getMessage());
			throw new AssertionError("User Password did not load  ", e);
		}
		return password;
	}
	
/*	public String enviromentUserURL(String url) {
		String envUrl = url;
		try {
			UserData uData = new UserData();
			System.out.println("User URL is " + url);

			switch (url) {
			case "stageUserURL":

				driver.get(uData.getStageUserURL());
				envUrl = uData.getStageUserURL();
				log.info("Navigated to " + uData.getStageUserURL());
				break;
			case "liveUserUrl":

				driver.get(uData.getLiveUserUrl());
				envUrl = uData.getLiveUserUrl();
				log.info("Navigated to " + uData.getLiveUserUrl());
				break;
			default:

				driver.get(uData.getStageUserURL());
				log.info("Navigated to " + uData.getStageUserURL());
				break;

			}
		} catch (Exception e) {

			System.out.println("FAILURE User URL did not load: " + url);
			test.log(LogStatus.ERROR, "User URL did not load because " + e.getMessage());
			throw new AssertionError("User URL did not load  ", e);
		}
		return envUrl;

	}*/

	public void SwitchTabandClose() {
		try {
			driver.close();
			test.log(LogStatus.INFO, "Tab switched and closed successfully");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Cannot switch Tab " + e.getMessage());
			throw new AssertionError("Cannot switch Tab ", e);
		}

	}

	public void inputTextEnter(String xpath, String value) {

		try {
			data = driver.findElement(By.xpath(Variables.OR.getProperty(xpath)));
			data.clear();
			String textvalue = value;
			System.out.println("textvalue " + textvalue);
			if (textvalue != null) {
				data.sendKeys(value);
				data.sendKeys(Keys.ENTER);
			} else {
				data.sendKeys(value);
				data.sendKeys(Keys.ENTER);
			}
			test.log(LogStatus.INFO, "Entered text is :: " + value);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Cannot Enter :: " + e.getMessage());
			throw new AssertionError("Cannot Enter ", e);
		}
	}

	public void inputText(String xpath, String value) {

		try {
			data = driver.findElement(By.xpath(Variables.OR.getProperty(xpath)));
			data.clear();

			String textvalue = value;
			System.out.println("textvalue " + textvalue);
			if (textvalue != null) {
				data.sendKeys(value);
				Thread.sleep(800);
			} else {
				data.sendKeys(value);
			}
			test.log(LogStatus.INFO, "Entered text is :: " + value);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Cannot Enter :: " + e.getMessage());
			throw new AssertionError("Cannot Enter ", e);
		}

	}

	public void inputTextWexpath(String xpath, String value) {
		try {

			String xpathforData = ".//input[@id='" + xpath + "']";
			scrollTillElementforCF(xpathforData);
			data = driver.findElement(By.xpath(xpathforData));
			data.clear();

			String textvalue = value;
			System.out.println("textvalue " + textvalue);
			if (textvalue != null) {
				data.sendKeys(value);
				Thread.sleep(300);
			} else {
				data.sendKeys(value);
			}
			test.log(LogStatus.INFO, "Entered text is :: " + value);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Cannot Enter :: " + e.getMessage());
			throw new AssertionError("Cannot Enter ", e);
		}

	}

	public void selectRadioOrCheckbox(By selector) {

		try {
			System.out.println("Select Radio Button / Checkbox");
			WebElement checkBox1;
			checkBox1 = driver.findElement(selector);

			if (!checkBox1.isSelected()) {
				checkBox1.click();
				test.log(LogStatus.INFO, checkBox1 + " Checked / Selected");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Cannot Check/Select " + e.getMessage());
			System.err.println("Element " + e.getMessage());
			throw new AssertionError("Cannot check/ Select Checkbox/ Select ", e);
		}
	}

	public void verifyRadioButton(String selector) {

		try {
			System.out.println("Verify Radiobutton Selected or not");
			Boolean radioSelected = driver.findElement(By.xpath(Variables.OR.getProperty(selector))).isSelected();
			System.out.println("radioSelected" + radioSelected);
			if (radioSelected.equals("true")) {
				System.out.println("Radio button is Selected");
				test.log(LogStatus.INFO, "Radio button is selected " + selector);
			} else {
				test.log(LogStatus.ERROR, "Radio button is not selected :: " + selector);
				throw new AssertionError("Radio button is not selected ");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Cannot Check/Select " + e.getMessage());
			System.err.println("Element " + e.getMessage());
			throw new AssertionError("Cannot check/ Select Checkbox/ Select ", e);
		}

	}

	public void selectFromDropdown(String xpath, String value) {

		try {

			System.out.println("select element from dropwown list");
			Thread.sleep(2000);
			Select dropdown = new Select(driver.findElement(By.xpath(Variables.OR.getProperty(xpath))));
			dropdown.getOptions();
			System.out.println("dropdown.getOptions()" + dropdown.getOptions());
			dropdown.selectByValue(value);
			Thread.sleep(2000);
			System.out.println(value + " is Selected");
			log.info(value + " is Selected");

		} catch (Exception e) {
			e.printStackTrace();
			log.info(value + " is not Selected");
			System.err.println(value + " is not selected");
			throw new AssertionError(value + " is not selected");
		}
	}

	public void selectFromDropdownForMapping(String xpath, String value) {

		try {

			System.out.println("select element from dropwown list");
			Thread.sleep(2000);
			Select dropdown = new Select(driver.findElement(By.xpath(xpath)));
			dropdown.getOptions();
			System.out.println("dropdown.getOptions()" + dropdown.getOptions());
			dropdown.selectByValue(value);
			Thread.sleep(2000);
			System.out.println(value + " is Selected");
			log.info(value + " is Selected");

		} catch (Exception e) {
			e.printStackTrace();
			log.info(value + " is not Selected");
			System.err.println(value + " is not selected");
			throw new AssertionError(value + " is not selected");
		}
	}

	public void verifyChatBackend(String value) throws Exception {

		try {
			//String xpath1 = ".//div[@class='msg backend isLast']//following::div[contains(text(),'" + value
			//		+ "')][2]";
			String xpath1 = ".//div[@class='visitor_msg']//following::div[contains(text(),'" + value
					+ "')]";
			System.out.println("Xpath for backend chat:" + xpath1);
			WebElement element = driver.findElement(By.xpath(xpath1));
			element.isDisplayed();
			test.log(LogStatus.INFO, "Chat message verification successfull at Back end :: " + value);

		} catch (Exception e) {
			test.log(LogStatus.ERROR, "not verify chat at backend :: " + value);
			throw new AssertionError("Chat message not found at Back end", e);
		}

	}

	public void verifyChatFrontend(String value) throws Exception {

		try {
			// switchframeByXpath("frontchatIframe");
			System.out.println("Verify Chat message in Front chat widget");
			String xpath1 = ".//span[@class='linkify standard-widget agent' and contains(text(),'" + value + "')]";
			System.out.println("Xpath for backend chat:" + xpath1);

			WebElement element = driver.findElement(By.xpath(xpath1));
			element.isDisplayed();
			test.log(LogStatus.INFO, "Chat message verification successfull at Front end :: " + value);

		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Chat message not found at Front end :: " + value);
			throw new AssertionError("Chat message not found at Front end", e);
		}
	}

	public void windowsConfirmationAlert_accept() {
		try {
			Alert confirmationAlert = driver.switchTo().alert();
			String alertText = confirmationAlert.getText();
			System.out.println("Alert text is " + alertText);
			Thread.sleep(2000);
			confirmationAlert.accept();
			test.log(LogStatus.INFO, "Window Alert handled " + alertText);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			test.log(LogStatus.ERROR, "Unable to handle window alert");
			System.err.println("Unable to Handle Window alert :: " + e.getMessage());
			throw new AssertionError("Unable to handle window alert");
		}
	}

	public void click(String xpath) {

		try {
			driver.findElement(By.xpath(Variables.OR.getProperty(xpath))).click();
			System.out.println(xpath + "Button clicked");
			test.log(LogStatus.INFO, xpath + " clicked");
			Thread.sleep(800);
		} catch (Exception e) {
			System.err.println("Cannot Click " + e.getMessage());
			test.log(LogStatus.ERROR, "Unable to click on :: " + xpath + " Button");
			throw new AssertionError("Unable to click on ::  " + xpath + " Button", e);
		}
	}

	public void clickElement(String xpath) {

		try {
			driver.findElement(By.xpath(xpath)).click();
			System.out.println(xpath + "Button clicked");
			test.log(LogStatus.INFO, xpath + " clicked");
			Thread.sleep(800);
		} catch (Exception e) {
			System.err.println("Cannot Click " + e.getMessage());
			test.log(LogStatus.ERROR, "Unable to click on :: " + xpath + " Button");
			throw new AssertionError("Unable to click on ::  " + xpath + " Button", e);
		}

	}

	public void waitForElementToBeClickable(By selector) {
		try {
			wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.elementToBeClickable(selector));
			test.log(LogStatus.INFO, "Element found " + selector);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Element not found" + selector);
			throw new TestException("The element is not clickable: " + selector);
		}
	}

	public void waitforelement(String xpath, int sec) throws Exception {

		boolean foundElement = false;
		WebDriverWait wait = new WebDriverWait(driver, sec);
		try {
			System.out.println("Enters into waitforelement");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Variables.OR.getProperty(xpath))));
			foundElement = true;
			System.out.println("foundElement" + foundElement);
			System.out.println("===== WebDriver is visible======");
			test.log(LogStatus.INFO, "Element visible :: " + xpath);
			System.out.println("element is displayed");
		} catch (Exception e) {
			foundElement = false;
			System.err.println("Element not Found ");
			test.log(LogStatus.ERROR, "Element is not visible :: " + xpath);
			Thread.sleep(800);
			throw new AssertionError("Element not Found");
		}
	}

	public static void explicitwait(String xpath) {

		WebDriverWait w = new WebDriverWait(driver, 160);
		w.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

	}
	public void waitforelementforMapping(String xpath, int sec) throws Exception {

		boolean foundElement = false;
		WebDriverWait wait = new WebDriverWait(driver, sec);
		try {
			System.out.println("Enters into waitforelement");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			foundElement = true;
			System.out.println("foundElement" + foundElement);
			System.out.println("===== WebDriver is visible======");
			test.log(LogStatus.INFO, "Element visible :: " + xpath);
			System.out.println("element is displayed");
		} catch (Exception e) {
			foundElement = false;
			System.err.println("Element not Found ");
			test.log(LogStatus.ERROR, "Element is not visible :: " + xpath);
			Thread.sleep(800);
			throw new AssertionError("Element not Found");
		}
	}

	public void escapeBrowserNotification() throws Exception {

		try {

			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ESCAPE);
			r.keyRelease(KeyEvent.VK_ESCAPE);
			System.out.println("Browser notofication Escaped");

		} catch (Exception e) {
			System.out.println("Notification can not be escaped " + e.getMessage());
			throw new AssertionError("Notification can not be escaped " + e.getMessage());
		}
	}

	public static void zip(String filepath) {
		try {
			File inFolder = new File(filepath);
			File outFolder = new File("Reports.zip");
			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outFolder)));
			BufferedInputStream in = null;
			byte[] data = new byte[1000];
			String files[] = inFolder.list();
			for (int i = 0; i < files.length; i++) {
				in = new BufferedInputStream(new FileInputStream(inFolder.getPath() + "/" + files[i]), 1000);
				out.putNextEntry(new ZipEntry(files[i]));
				int count;
				while ((count = in.read(data, 0, 1000)) != -1) {
					out.write(data, 0, count);
				}
				out.closeEntry();
			}
			out.flush();
			out.close();

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public void switchToMainFrame() {

		try {

			System.out.println("Before switching -- Switch to Main Frame");
			driver.switchTo().defaultContent();
			System.out.println("After switching -- Switch to Main Frame");
			test.log(LogStatus.INFO, "Switched to main Frame ");
			Thread.sleep(800);

		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Can not switch to main frame ");
			System.err.println("Can not Switch " + e.getMessage());
			throw new AssertionError("Switched to main Frame Error");
		}

	}

	public static void delete(final File folder) throws FileNotFoundException {
		System.out.println("Enteres into Delete Function");
		try {
			if (folder.isDirectory()) { // Check if folder file is a real folder
				File[] list = folder.listFiles(); // Storing all file name within array
				System.out.println("list of Files" + list.length);
				System.out.println("list of Files not null");// Checking list value is null or not to check folder
																// containts atlest one file
				for (int i = 0; i < list.length; i++) {
					File tmpF = list[i];
					System.out.println("list[i]" + list[i]);
					if (tmpF.getName().endsWith(".jpg")) {
						if (tmpF.isDirectory()) { // if folder found within folder remove that folder using recursive
													// method
							delete(tmpF);
						}
						tmpF.delete();
					} // else delete file
				}
				if (!folder.delete()) { // if not able to delete folder print message
					System.out.println("can't delete folder : " + folder);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
			e.printStackTrace();
		}

	}

	public void verifyText(String xpath, String data) {

		String text;
		boolean res;
		try {
			System.out.println("Enters into verify Text");
			text = driver.findElement(By.xpath(Variables.OR.getProperty(xpath))).getText();
			System.out.println("text " + text);
			if (data.isEmpty()) {
				System.out.println("data is empty");
			} else {
				if (text.equalsIgnoreCase(data)) {
					res = true;
					System.out.println(data + " is Displayed " + res);
					test.log(LogStatus.INFO, "Text is verified and displayed as :: " + data);
				} else {
					res = false;
					System.err.println(data + " is not Displayed " + res);
					test.log(LogStatus.ERROR, "Text is not verified and displayed :: " + data);
					throw new AssertionError(data + " is not displayed");
				}
			}

		} catch (Exception e) {

			System.err.println("Data " + e.getMessage());
			test.log(LogStatus.ERROR, "Text is not verified and displayed :: " + data);
			throw new AssertionError(data + " is not displayed");
		}
	}

	public String getallText(String xpath) {

		String text;

		try {
			System.out.println("Enters into verify Text");
			text = driver.findElement(By.xpath(Variables.OR.getProperty(xpath))).getText();
			System.out.println("all Description text " + text);

		} catch (Exception e) {

			System.err.println("Data " + e.getMessage());
			test.log(LogStatus.ERROR, "Text is not verified and displayed :: " + data);
			throw new AssertionError(data + " is not displayed");
		}
		return text;
	}

	public void verifyTextFromInput(String xpath, String data) {

		String text;
		boolean res;
		try {
			System.out.println("Enters into verify Text");
			text = driver.findElement(By.xpath(Variables.OR.getProperty(xpath))).getAttribute("value");
			System.out.println("text " + text);
			if (data.isEmpty()) {
				System.out.println("data is empty");
			} else {
				if (text.equalsIgnoreCase(data)) {
					res = true;
					System.out.println(data + " is Displayed " + res);
					test.log(LogStatus.INFO, "Text is verified and displayed as :: " + data);
				} else {
					res = false;
					System.err.println(data + " is not Displayed " + res);
					test.log(LogStatus.ERROR, "Text is not verified and displayed :: " + data);
					throw new AssertionError(data + " is not displayed");
				}
			}

		} catch (Exception e) {

			System.err.println("Data " + e.getMessage());
			test.log(LogStatus.ERROR, "Text is not verified and displayed :: " + data);
			throw new AssertionError(data + " is not displayed");
		}
	}

	public void verifyTextwithTrim(String xpath, String data) {

		String text;
		String filterdString;
		boolean res;
		try {
			System.out.println("Enters into verify Text");
			text = driver.findElement(By.xpath(Variables.OR.getProperty(xpath))).getText();
			filterdString = text.replaceAll("[^a-zA-Z]+", "");
			System.out.println("text " + filterdString);
			if (data.isEmpty()) {
				System.out.println("data is empty");
			} else {
				Thread.sleep(1000);
				if (filterdString.equalsIgnoreCase(data)) {
					res = true;
					System.out.println(data + " is Displayed " + res);
					test.log(LogStatus.INFO, "Text is verified and displayed as :: " + data);
				} else {
					res = false;
					System.err.println(data + " is not Displayed " + res);
					test.log(LogStatus.ERROR, "Text is not verified and displayed :: " + data);
					throw new AssertionError(data + " is not displayed");
				}
			}

		} catch (Exception e) {

			System.err.println("Data " + e.getMessage());
			test.log(LogStatus.ERROR, "Text is not verified and displayed FALSE :: " + e);
			throw new AssertionError(e + " DATA is not displayed");
		}
	}

	public void verifyAngularElement(String elementName, String data, String elementby) {

		boolean res;
		try {
			Thread.sleep(2000);
			System.out.println("Enters into Verify Angular Element");
			if (data.isEmpty()) {
				System.out.println("data is empty");
			} else {
				if (driver instanceof JavascriptExecutor) {
					JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
					if (elementby.equalsIgnoreCase("Name")) {
						text = (String) javascriptExecutor
								.executeScript("return document.getElementsByName('" + elementName + "')[0].value;");
					}
					if (elementby.equalsIgnoreCase("Id")) {
						text = (String) javascriptExecutor
								.executeScript("return document.getElementById('" + elementName + "').value;");
					}
					if (elementby.equalsIgnoreCase("TagName")) {
						text = (String) javascriptExecutor
								.executeScript("return document.getElementsByTagName('" + elementName + "')[0].value;");
					}

					System.out.println("text is " + text);
					if (text.equalsIgnoreCase(data)) {
						res = true;
						System.out.println("Angular Element " + data + " is Displayed " + res);
						test.log(LogStatus.INFO, "Angular Element is verified and displayed as :: " + data);
					} else {
						res = false;
						System.err.println(data + " is not Displayed " + res);
						test.log(LogStatus.ERROR, "Angular Element is not verified and displayed :: " + data);
						throw new AssertionError("Angular Element " + data + " is not displayed");
					}
				}
			}

		} catch (Exception e) {
			System.err.println("Data " + e.getMessage());
			test.log(LogStatus.ERROR, "Text is not verified and displayed :: " + data);
			throw new AssertionError("Angular Element " + data + " is not displayed");
		}
	}

	public boolean verifyAngularElementForSalesforce(String elementName, String elementby, int i) {

		boolean res;
		boolean status = false;
		try {
			Thread.sleep(2000);
			System.out.println("Enters into Verify Angular Element");
			if (driver instanceof JavascriptExecutor) {
				JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
				if (elementby.equalsIgnoreCase("Name")) {
					status = (boolean) javascriptExecutor.executeScript(
							"return document.getElementsByName('" + elementName + "')[" + i + "].checked;");
					System.out.println("verifyAngularElementForSalesforce: " + status);
				}

				System.out.println("text is :::::::::" + status);
				if (status == true) {
					res = true;
					System.out.println("Angular Element " + elementName + " is Displayed " + res);
					test.log(LogStatus.INFO, "Angular Element is verified and displayed as :: " + elementName);
				} else {
					res = false;
					System.err.println(elementby + " is not Displayed " + res);
					test.log(LogStatus.INFO, "Element is not checked :: " + elementName);
				}
			}

		} catch (Exception e) {
			System.err.println("Data " + e.getMessage());
			test.log(LogStatus.ERROR, "Text is not verified and displayed :: " + elementby);
			throw new AssertionError("Angular Element " + elementby + " is not displayed");
		}
		return status;
	}

	public void scrollTillElement(String xpath) {
		try {
			System.out.println("in Scrolling");
			WebElement element = driver.findElement(By.xpath(Variables.OR.getProperty(xpath)));
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.perform();
			Thread.sleep(1000);
			test.log(LogStatus.INFO, "Scrolled and element found");
			System.out.println("Scrolling Done");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Scrolled and element not found");
			System.out.println("Fail " + e.getMessage());
			System.out.println("Scrolling Done");
			throw new AssertionError("Scrolling not working");
		}
	}

	public void scrollTillElementforCF(String xpath) throws Exception {

		Point hoverItem = driver.findElement(By.xpath(xpath)).getLocation();
		((JavascriptExecutor) driver).executeScript("return window.title;");
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0," + (hoverItem.getY() - 400) + ");");
	}

	public void scrollTillElementForMapping(String xpath) {
		try {
			System.out.println("eneter into scroll up");
			WebElement element = driver.findElement(By.xpath((xpath)));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", element);
			Thread.sleep(500);
			test.log(LogStatus.INFO, "Scrolled and element found");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Scrolled and element not found");
			System.out.println("Fail " + e.getMessage());
			throw new AssertionError("Scrolling not working");
		}
	}

	public static boolean deleteDirectory(File dir) {
		if (dir.isDirectory()) {
			File[] children = dir.listFiles();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDirectory(children[i]);
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

	public void openNewTab(String url) {
		try {
			((JavascriptExecutor) driver).executeScript("window.open('" + url + "');");
			test.log(LogStatus.INFO, "URL Opened in new Tab :: " + url);
		} catch (Exception e) {
			System.err.println(url + " is not open in new Tab ");
			test.log(LogStatus.ERROR, "URL is not opened in new Tab :: " + url);
			throw new AssertionError(url + " is not opened");
		}

	}

	public void verifyEquals(String expected, String actual) throws IOException {
		try {
			Thread.sleep(800);
			Assert.assertEquals(expected, actual);
			test.log(LogStatus.INFO,
					"Verification successfull Actual Page Title is ::  " + actual + " and Expected is :: " + expected);
		} catch (Throwable t) {
			test.log(LogStatus.ERROR, " Verification failed with exception :: " + t.getMessage());
			System.out.println("Fail " + t.getMessage());
			throw new AssertionError("Verification failed");
		}
	}

	public void waitForElementToBeVisible(By selector) {

		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.presenceOfElementLocated(selector));
			test.log(LogStatus.INFO, "Element is Visible :: " + selector);

		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Element is not Visible :: " + selector);
			throw new NoSuchElementException(String.format("Element was not visible:: ", selector));
		}
	}

	public WebElement getElement(By selector) {

		try {
			return driver.findElement(selector);
		} catch (Exception e) {
			System.out.println(String.format("Element %s does not exist - proceeding", selector));
			test.log(LogStatus.ERROR, "Element does not exist :: " + selector);
		}

		return null;
	}

	public void sendKeys(By selector, String value) {
		WebElement element = getElement(selector);
		clearField(element);
		try {
			element.sendKeys(value);
			test.log(LogStatus.INFO, "Successfully entered :: " + value);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Error in Entering value :: " + value);
			throw new TestException(
					String.format("Error in sending [%s] to the following element: [%s]", value, selector.toString()));
		}
	}

	public void sendKeysWithEnter(By selector, String value) {
		WebElement element = getElement(selector);
		clearField(element);
		try {
			element.sendKeys(value);
			element.sendKeys(Keys.ENTER);
			test.log(LogStatus.INFO, "Successfully entered :: " + value);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Error in Entering value :: " + value);
			throw new TestException(
					String.format("Error in sending [%s] to the following element: [%s]", value, selector.toString()));
		}
	}

	public void clearField(WebElement element) {
		try {
			String text = element.getText();
			if (text.length() > 0) {
				element.clear();
				waitForElementTextToBeEmpty(element);
			}

		} catch (Exception e) {
			System.out.print(String.format("The following element could not be cleared: [%s]", element.getText()));
			test.log(LogStatus.ERROR, "The following element could not be cleared :: " + element);
		}
	}

	public void waitForElementTextToBeEmpty(WebElement element) {

		String text;
		try {
			text = element.getText();
			int maxRetries = 10;
			int retry = 0;
			while ((text.length() >= 1) || (retry < maxRetries)) {
				retry++;
				text = element.getText();
			}
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Error in Text to be empty :: " + element);
			throw new TestException(
					String.format("Error in Text to be empty [%s] to the following element: [%s]", element.toString()));
		}
	}

	public void clickButton(By selector) {
		WebElement element = getElement(selector);
		waitForElementToBeClickable(selector);
		try {
			element.click();
			Thread.sleep(800);
			test.log(LogStatus.INFO, "Button clicked :: " + selector);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "The Element could not be clickable :: " + element);
			throw new TestException(String.format("The following element is not clickable: [%s]", selector));
		}
	}

	public void clickNewConvButton(String xpath) {

		System.out.println("Click on New conv button if Exists");
		WebElement newConvButton;
		newConvButton = driver.findElement(By.xpath(Variables.OR.getProperty(xpath)));
		if (newConvButton.isDisplayed() == true) {
			newConvButton.click();
			test.log(LogStatus.INFO, "Button clicked :: " + xpath);
		} else {
			System.out.println("New Conversation button not displayed");
			test.log(LogStatus.ERROR, "The Element could not be clickable :: " + xpath);
		}
	}

	public void waitForelementToDisplay(By selector) {

		WebElement element = getElement(selector);
		try {
			while (!element.isDisplayed()) {
				System.out.println("Waiting for element to display: " + selector);
				sleep(200);
			}
			test.log(LogStatus.INFO, "Element is displayed :: " + selector);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Element is not displayed :: " + selector);
			throw new AssertionError("Element is not displayed" + selector);
		}

	}

	public void sleep(final long millis) {

		System.out.println((String.format("Sleeping %d ms ", millis)));
		try {
			Thread.sleep(millis);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}

	public Object openNewTabNew(Object enviromentUserURL) {
		try {
			((JavascriptExecutor) driver).executeScript("window.open('" + enviromentUserURL + "');");
			test.log(LogStatus.INFO, "URL Opened in new Tab :: " + enviromentUserURL);
		} catch (Exception e) {
			System.err.println(enviromentUserURL + " is not open in new Tab ");
			test.log(LogStatus.ERROR, "URL is not opened in new Tab :: " + enviromentUserURL);
			throw new AssertionError(enviromentUserURL + " is not opened");
		}
		return enviromentUserURL;
	}

	public void uploadFileWithRobot(String imagePath) {
		StringSelection stringSelection = new StringSelection(imagePath);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, null);

		Robot robot = null;

		try {
			robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}

		robot.delay(250);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(150);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public void UploadFileUsingSendKeys(String imagePath) throws InterruptedException {

		String filepath = imagePath;

		WebElement fileInput = driver.findElement(By.xpath(Variables.OR.getProperty("fileUploadVisitor")));
		fileInput.sendKeys(filepath);
		// Added a wait to make you notice the difference.
		Thread.sleep(1000);
	}

	public String readPDFInURL(String text) throws EmptyFileException, IOException {
		System.out.println("Enters into READ PDF");
		String output = "";
		URL url = new URL(driver.getCurrentUrl());
		System.out.println("url :  " + url);
		InputStream is = url.openStream();
		BufferedInputStream fileToParse = new BufferedInputStream(is);
		PDDocument document = null;
		try {
			document = PDDocument.load(fileToParse);
			output = new PDFTextStripper().getText(document);
			if (output.contains(text)) {
				System.out.println("Element is matched in PDF is : " + text);
				test.log(LogStatus.INFO, "Element is displayed in PDF " + text);
			} else {
				System.out.println("Element is not  matched in PDF");
				test.log(LogStatus.ERROR, "Element is not displayed in PDF :: " + text);
				throw new AssertionError("Element is not displayed" + text);
			}
		} finally {
			if (document != null) {
				document.close();
			}
			fileToParse.close();
			is.close();
		}
		return output;
	}

	public void closechatIfExist(By selector) throws Exception {

		System.out.println("enters into Check chat stage");
		List<WebElement> selector1 = driver.findElements(selector);
		int xpathCount = selector1.size();
		System.out.println("Total xpath: " + xpathCount);

		if (xpathCount > 0) {

			Iterator<WebElement> itr = selector1.iterator();
			while (itr.hasNext()) {
				itr.next().click();
				System.out.println("close chat");
				commonUtil.switchframeByXpath("dashClosePopupIframe");
				commonUtil.click("dashBtnClosePopup");
			}
		} else {
			System.out.println("No chat is in Open state");
		}
	}

	public void closePendingChat(By pending, By current) throws Exception {

		System.out.println("enters into Check Pending chat");
		List<WebElement> selector2 = driver.findElements(pending);
		int xpathCount = selector2.size();
		System.out.println("Total xpath: " + xpathCount);

		if (xpathCount > 0) {

			Iterator<WebElement> itr1 = selector2.iterator();
			while (itr1.hasNext()) {
				itr1.next().click();
				System.out.println("close Pending chat");
				closechatIfExist(current);

			}
		} else {
			System.out.println("No chat is in Open state");
		}
	}

	public void loginToChatProcess() throws Exception {
		
		String visitorName = ".//input[@name ='visitor_name']";
	    String visitorEmail = ".//input[@name ='visitor_email']";
	    String startConBtn = ".//span[contains(text(),'Start Conversation')]";
		String fName = faker.name().fullName();
		String comapnyName = faker.company().name().replaceAll("[\\s\\'\\.\\^:,]", "").toLowerCase();
		String firstName = fName.replaceAll("[\\s\\'\\.\\^:,]", "").toLowerCase();
		
		loginPage.login(userName(uData.getUserEmail()), userPassword(uData.getUserPassword()));
		Assert.assertTrue(homePage.verifyUsername());
		openNewWindow(uData.getUserUrl());
		switchTab("1");
		Thread.sleep(2000);
		switchframeByXpath("frontChatWidgetIframe");
		homePage.clickFlotingchatBtn();
		commonUtil.switchToMainFrame();
		switchframeByXpath("frontchatIframe");
		Thread.sleep(2000);
		// commonUtil.verifyText("frontverifyconv", "Your conversations");
		// commonUtil.waitforelement("frontNewconvBtn", 10);
		// commonUtil.click("frontNewconvBtn");
		// homePage.clickNewConvBtn();
		explicitwait(visitorName);
		driver.findElement(By.xpath(visitorName)).sendKeys(firstName);
		explicitwait(visitorEmail);
		driver.findElement(By.xpath(visitorEmail)).sendKeys(firstName + "@" + comapnyName + ".com");
		explicitwait(startConBtn);
		driver.findElement(By.xpath(startConBtn)).click();
		homePage.enterTextFrontend(faker.address().country()
				+ faker.address().streetName() + faker.address().secondaryAddress());
		Thread.sleep(2000);
		
		homePage.enterTextFrontend("hello how are you");
		 switchToMainFrame();
		switchTab("0");
		homePage.clickAnsChatBtn();
	}

	public void visitorWindowChat() throws Exception {

		openNewTab(uData.getUserUrl());
		switchTab("1");
		switchframeByXpath("frontChatWidgetIframe");
		homePage.clickFlotingchatBtn();
		commonUtil.switchToMainFrame();
		switchframeByXpath("frontchatIframe");
		commonUtil.verifyText("frontverifyconv", "Your conversations");
		commonUtil.waitforelement("frontNewconvBtn", 10);
		commonUtil.click("frontNewconvBtn");
		// homePage.clickNewConvBtn();
		homePage.enterTextFrontend("hello how");
		// switchToMainFrame();
		switchTab("0");
		homePage.clickAnsChatBtn();
	}

	public void logoutProcess() throws Exception {

		// homePage.closeChatFrontend();
		SwitchTabandClose();
		switchTab("0");
		homePage.closeChatBackend();
		switchToMainFrame();
		// Thread.sleep(3000);
		switchframeByXpath("dashClosePopupIframe");
		click("dashBtnClosePopup");
		loginPage.clickLinkforLogout();
		loginPage.clickLogout();

	}

	public static void makeDirectory() {

		System.out.println("Enter into Make diretory");
		String directories = System.getProperty("user.dir") + "\\target\\surefire-reports\\AcquireTest";
		File file = new File(directories);
		boolean result = file.mkdirs();
		System.out.println("Status = " + result);
	}

	public void verifyCheckboxvalue(String element) {
		WebElement webElement = driver.findElement(By.xpath(Variables.OR.getProperty(element)));
		if (webElement.isSelected()) {
			System.out.println(element + " Checkbox is Selected TRUE");
			test.log(LogStatus.INFO, "Checkbox is Selected TRUE " + element);
		} else {
			System.out.println(element + " Checkbox is Selected FALSE");
			test.log(LogStatus.ERROR, "Checkbox is Selected FALSE " + element);
			throw new AssertionError("Checkbox is Selected FALSE " + element);
		}
	}

	public static void waitForPageLoad() {

		System.out.println("Enters into Page load");
		Wait<WebDriver> wait = new WebDriverWait(driver, 30);
		wait.until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver) {
				System.out.println("Current Window State       : "
						+ String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState")));
				return String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
						.equals("complete");
			}
		});
	}

	public static void waitforIframeLoad(String xpath) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(Variables.OR.getProperty(xpath)));
			System.out.println(xpath + " IFrame Loaded TRUE");
			test.log(LogStatus.INFO, "IFrame Loaded TRUE " + xpath);
		} catch (Exception e) {
			System.out.println(xpath + " IFrame Loaded FALSE");
			test.log(LogStatus.ERROR, "IFrame Loaded FALSE " + xpath);
			throw new AssertionError("IFrame Loaded FALSE " + xpath);
		}

	}

	public void verifyPageTitle(String pageTtile) {
		try {
			String actualTitle = driver.getTitle();
			String expectedTitle = pageTtile;
			assertEquals(expectedTitle, actualTitle);
			System.out.println("Page Title Matched");
			test.log(LogStatus.INFO, "Page Title Displayed TRUE " + pageTtile);
		} catch (Exception e) {
			System.out.println(pageTtile + "PageTitle Displayed False");
			test.log(LogStatus.ERROR, "PageTitle Displayed False" + pageTtile);
			throw new AssertionError("PageTitle Displayed False" + pageTtile);
		}
	}

	public void checkFieldDisplayandMap(String data, String value, String elementName, String elementData, String by) {
		try {
			scrollTillElement(data);
			commonUtil.waitforelement(data, 10);
			commonUtil.verifyTextwithTrim(data, value);
			commonUtil.verifyAngularElement(elementName, elementData, by);
			test.log(LogStatus.INFO, data + " Field is Displayed");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, data + "Field is not Displayed");
			throw new AssertionError(data + " Field is not Displayed", e);
		}

	}

	public void mappContactFields(String data, String value) {

		try {
			String xpathforData = ".//td[@id='field-" + data + "']";
			System.out.println("xpathforData: " + xpathforData);
			String xpathForDropdown = ".//select[@id='field-select-" + data + "']";
			System.out.println("xpathForDropdown: " + xpathForDropdown);
			scrollTillElementForMapping(xpathforData);
			waitforelementforMapping(xpathforData, 10);
			// commonUtil.verifyText(data, value);
			commonUtil.selectFromDropdownForMapping(xpathForDropdown, value);
			test.log(LogStatus.INFO, data + "Field displayed and slected PASS");
		} catch (Exception e) {
			test.log(LogStatus.ERROR, data + "Field displayed and slected FAIL");
			throw new AssertionError(data + "Field displayed and slected FAIL", e);
		}

	}

	@SuppressWarnings("unused")
	public void fakeDetailGenerator(String data, String value) {

		Faker faker = new Faker();

		String name = faker.name().fullName();
		String firstName = faker.name().firstName();
		String lastName = faker.name().lastName();

		String streetAddress = faker.address().streetAddress();

	}

	public List<String> extractTag() throws IOException {

		String fileName = Variables.salesforceContactFile;
		List<String> tagItems = new ArrayList<String>();
		List<String> removeItems = new ArrayList<String>();
		File file = new File(fileName);

		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		System.out.println("Reading text file using FileReader");
		while ((line = br.readLine()) != null) {
			Pattern pattern = Pattern.compile("<option.*>(.+?)</option>");
			Matcher matcher = pattern.matcher(line);

			while (matcher.find()) {
				String anchorText = matcher.group(1);
				tagItems.add(anchorText.trim().replace(' ', '-').toLowerCase());
				System.out.println("Fields = " + tagItems);
			}
			removeItems.add("last-name");
			removeItems.add("department");
			removeItems.add("other-street");
			removeItems.add("account");
			removeItems.add("email");
			removeItems.add("phone");

			for (String check : removeItems) {
				if (tagItems.contains(check))
					tagItems.remove(check);
			}
		}
		System.out.println("Final Fields after Remove = " + tagItems);
		System.out.println("Final Fields Count after Delete= " + tagItems.size());
		br.close();
		fr.close();
		return tagItems;
	}

	public void deleteExistingField() throws Exception {

		extractTag();
		driver.get("https://app.acquire.io/widget/form");
		Thread.sleep(1000);
		scrollTillElementforCF(Variables.OR.getProperty("createCustomField"));
		Boolean isPresent = driver.findElements(By.xpath(Variables.OR.getProperty("deletecustomField"))).size() > 0;
		System.out.println("status" + isPresent);
		if (isPresent.equals(true)) {
			System.out.println("Element is Visible");
			scrollTillElementforCF(Variables.OR.getProperty("deletecustomField"));
			List<WebElement> allElements = driver.findElements(By.xpath(Variables.OR.getProperty("deletecustomField")));
			int s = allElements.size();

			System.out.println("Total Element for Remove is: " + s);

			for (int i = 0; i < s; i++) {
				Thread.sleep(1000);
				allElements.get(i).click();
				Thread.sleep(1000);
				WebElement popup = driver.findElement(By.xpath(Variables.OR.getProperty("confirmDeleteYes")));
				popup.click();
				Thread.sleep(1000);
			}
		} else {
			System.out.println("Element is InVisible");
		}
	}

	public void createandEditCustomField() throws Exception {

		List<String> tagItems = extractTag();
		for (int i = 1; i < tagItems.size(); i++) {
			Thread.sleep(1000);
			driver.findElement(By.id("addonlinefield")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath(Variables.OR.getProperty("createNewCustomField"))).click();
			Thread.sleep(1000);
			List<WebElement> allElement = driver.findElements(By.xpath(Variables.OR.getProperty("editCustomField")));
			List<WebElement> allNameElement = driver.findElements(By.xpath(Variables.OR.getProperty("editName")));
			List<WebElement> allLabelElement = driver.findElements(By.xpath(Variables.OR.getProperty("editLabel")));
			int count = allElement.size();
			int count1 = allLabelElement.size();
			int count2 = allNameElement.size();

			scrollTillElementforCF(Variables.OR.getProperty("deletecustomField"));
			allElement.get(count - 1).click();
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(allLabelElement.get(count1 - 1))).clear();
			// allLabelElement.get(count1 - 1).clear();
			allLabelElement.get(count1 - 1).sendKeys(tagItems.get(i));
			Thread.sleep(1000);
			allNameElement.get(count2 - 1).clear();
			allNameElement.get(count2 - 1).sendKeys(tagItems.get(i));
			driver.findElement(By.xpath(Variables.OR.getProperty("togglePncilIcon"))).click();
			Thread.sleep(1000);
		}
		driver.findElement(By.xpath(Variables.OR.getProperty("ongetData"))).click();
	}

	public String randomPhoneGenerate() {
		String phNo = null;
		try {

			String randomNumbers = RandomStringUtils.randomNumeric(8);
			phNo = "88" + randomNumbers;
			System.out.println("Unique Phone number is: " + phNo);

		} catch (Exception e) {
			System.out.println("Error in Phone " + phNo);
		}
		return phNo;
	}

	public String randomFaxGenerate() {

		String faxNo = null;
		try {

			String randomNumbers = RandomStringUtils.randomNumeric(7);
			faxNo = "+44 161" + randomNumbers;
			System.out.println("Unique Phone number is: " + faxNo);

		} catch (Exception e) {
			System.out.println("Error in Phone " + faxNo);
		}
		return faxNo;
	}

	public List<String> addSalesforceFields() throws IOException {

		List<String> addfields = new ArrayList<String>();
		addfields.clear();

		addfields.add(faker.code().gtin8());
		addfields.add(faker.name().firstName());
		addfields.add(faker.name().prefix());
		addfields.add(faker.address().city());
		addfields.add(faker.address().state());
		addfields.add(faker.address().zipCode());
		addfields.add(faker.address().country());
		addfields.add(faker.address().streetName());
		addfields.add(faker.address().cityName());
		addfields.add(faker.address().state());
		addfields.add("32462");
		addfields.add(faker.address().country().toLowerCase());
		addfields.add(randomFaxGenerate());
		addfields.add(randomPhoneGenerate());
		addfields.add(randomPhoneGenerate());
		addfields.add(randomPhoneGenerate());
		addfields.add(randomPhoneGenerate());
		addfields.add(faker.commerce().productName());
		addfields.add(faker.artist().name());
		addfields.add("1991-08-08");
		addfields.add(faker.address().fullAddress());
		addfields.add(faker.beer().name());
		addfields.add(faker.color().name());

		return addfields;

	}

	public void doubbleClick(String xpath) {

		try {
			Actions action = new Actions(driver);
			WebElement element = driver.findElement(By.xpath(Variables.OR.getProperty(xpath)));
			action.doubleClick(element).perform();
			System.out.println(xpath + " Doubble Click  TRUE");
			test.log(LogStatus.INFO, "Doubble Click TRUE " + xpath);
		} catch (Exception e) {
			System.out.println(xpath + " Doubble Click FAIL");
			test.log(LogStatus.ERROR, "Doubble click FAIL " + xpath);
			throw new AssertionError("Doubble click FAIL " + xpath);
		}

	}

	public void checkSFSyncSetting(String checkBox, String chatTransDropXpath, String transcriptDropDownValue,
			int number) {

		try {
			Boolean checkStatus = commonUtil.verifyAngularElementForSalesforce("chat_type", "Name", number);
			if (checkStatus == true) {
				System.out.println("Already Case checked:");
				test.log(LogStatus.INFO, "Already Checkbox Selected " + checkBox);
			} else {
				commonUtil.click(checkBox);
				commonUtil.selectFromDropdown(chatTransDropXpath, transcriptDropDownValue);
				System.out.println("Subject for Case Dropdown Selected");
				test.log(LogStatus.INFO, "Checkbox is selected as " + checkBox);
			}
		} catch (Exception e) {
			System.out.println("Fail to Sync Setting");
			test.log(LogStatus.ERROR, "Fail to Sync Setting" + e);
			throw new AssertionError("Fail to Sync Setting" + e);
		}
	}

	public void clickFeed(String checkBox, int number) {

		try {

			Boolean checkStatus = commonUtil.verifyAngularElementForSalesforce("chat_type", "Name", number);
			if (checkStatus == true) {
				System.out.println("Already Case checked:");
				test.log(LogStatus.INFO, "Already Checkbox Selected " + checkBox);
			} else {
				commonUtil.click(checkBox);
			}
		} catch (Exception e) {
			System.out.println("Fail to Sync Setting");
			test.log(LogStatus.ERROR, "Fail to Sync Setting" + e);
			throw new AssertionError("Fail to Sync Setting" + e);
		}

	}

	public void salesforceCaseSubject(String data) {

		try {
			System.out.println("Enters into Salesforce Case Subejct Selection: " + data);
			String caseSubjact = ".//h3[contains(text(),'Open Activities')]//preceding::a[contains(text(),'Chat Transcript-"
					+ data + "')]";
			List<WebElement> subjectTotal = driver.findElements(By.xpath(caseSubjact));
			System.out.println("CASE Subject Xpath is" + caseSubjact);
			subjectTotal.size();
			WebElement firstElement = (WebElement) Iterables.getFirst(subjectTotal,
					driver.findElements(By.xpath(caseSubjact)));
			System.out.println("firstElement" + firstElement);
			firstElement.click();
			System.out.println("Subject Is selected TRUE");
			test.log(LogStatus.INFO, "Subject Selected " + caseSubjact);
		} catch (Exception e) {
			System.out.println("Fail to Click Subject");
			test.log(LogStatus.ERROR, "Fail to Click subject" + e);
			throw new AssertionError("Fail to Click Subject" + e);
		}
	}

	public void salesforceNoteSubject(String data) {

		try {
			System.out.println("Enters into Salesforce Note Subejct Selection: " + data);
			String noteSubjact = ".//h3[contains(text(),'Campaign History')]//following::a[contains(text(),'Chat Transcript-"
					+ data + "')";
			List<WebElement> notesubjectTotal = driver.findElements(By.xpath(noteSubjact));
			System.out.println("Note Subject Xpath is" + noteSubjact);
			notesubjectTotal.size();
			WebElement firstElement = (WebElement) Iterables.getFirst(notesubjectTotal,
					driver.findElements(By.xpath(noteSubjact)));
			System.out.println("firstElement" + firstElement);
			// firstElement.click();
			System.out.println("Note Subject Is displayed TRUE");
			test.log(LogStatus.INFO, "Subject Selected " + noteSubjact);
		} catch (Exception e) {
			System.out.println("Fail to Click Note Subject");
			test.log(LogStatus.ERROR, "Fail to Click Note subject" + e);
			throw new AssertionError("Fail to Click Note Subject" + e);
		}
	}

	public List<String> allchatMessages() {
		List<String> addfields = new ArrayList<String>();
		try {
			System.out.println("Enters into Chat message check");
			// List<WebElement> allElements =
			// driver.findElements(By.xpath(Variables.OR.getProperty("chatMessages")));

			// Get the rows which change always as and when users are added

			List<WebElement> elements = driver.findElements(By.className("visitor_msg"));
			System.out.println("Number of elements:" + elements.size());

			for (int i = 0; i < elements.size(); i++) {
				System.out.println(elements.get(i).getText());
				addfields.add(elements.get(i).getText());
				addfields.removeAll(Arrays.asList("", null));
				System.out.println("Final Fields for TEST Messages :: " + addfields);

			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return addfields;

	}

	// #------------------------ New Methods for POM
	// -------------------------------------------#

	public void waitforanElement(By xpath, int sec) throws Exception {

		boolean foundElement = false;
		WebDriverWait wait = new WebDriverWait(driver, sec);
		try {
			System.out.println("Enters into waitforelement");
			wait.until(ExpectedConditions.visibilityOfElementLocated(xpath));
			foundElement = true;
			System.out.println("foundElement" + foundElement);
			System.out.println("===== WebDriver is visible======");
			test.log(LogStatus.INFO, "Element visible :: " + xpath);
			System.out.println("element is displayed");
		} catch (Exception e) {
			foundElement = false;
			System.err.println("Element not Found ");
			test.log(LogStatus.ERROR, "Element is not visible :: " + xpath);
			Thread.sleep(800);
			throw new AssertionError("Element not Found");
		}
	}

	public void inputText1(By xpath, String value) {

		try {
			data = driver.findElement(xpath);
			data.clear();

			String textvalue = value;
			System.out.println("textvalue " + textvalue);
			if (textvalue != null) {
				data.sendKeys(value);
				Thread.sleep(800);
			} else {
				data.sendKeys(value);
			}
			test.log(LogStatus.INFO, "Entered text is :: " + value);
		} catch (Exception e) {
			test.log(LogStatus.ERROR, "Cannot Enter :: " + e.getMessage());
			throw new AssertionError("Cannot Enter ", e);
		}

	}

	public void fillAttributeName(By xpath, By btnxpath, By valueXpath) {
		System.out.println("Enters into Atribute name");
		List<WebElement> allElements = driver.findElements(xpath);
		int s = allElements.size();

		System.out.println("Total Element is: " + s);

		for (int i = 0; i < s; i++) {

			allElements.get(i).click();
			allElements.get(i).sendKeys("firstName");
			inputText1(valueXpath, "user.firstName");

			commonUtil.clickButton(btnxpath);
			allElements.get(i).sendKeys("lastName");
			inputText1(valueXpath, "user.lastName");
			System.out.println("loop count : " + i);
		}

	}

	public void SwitchToAnotherWindow(WebDriver driver, int window_number) {
		System.out.println("switch to window");
		List<String> windowlist = null;

		Set<String> windows = driver.getWindowHandles();
		System.out.println("widnows total  " + windows.size());
		windowlist = new ArrayList<String>(windows);

		String currentWindow = driver.getWindowHandle();

		if (!currentWindow.equalsIgnoreCase(windowlist.get(window_number - 1))) {
			driver.switchTo().window(windowlist.get(window_number - 1));
		}

	}
	
	public static void compareTwoImages() {
		//load images to be compared:
		
        BufferedImage expectedImage = ImageComparisonUtil.readImageFromResources("Screenshot_220.jpg");
        BufferedImage actualImage = ImageComparisonUtil.readImageFromResources("Screenshot_222.jpg");

        // where to save the result (leave null if you want to see the result in the UI)
        File resultDestination = new File( "result.png" );

        //Create ImageComparison object with result destination and compare the images.
        ImageComparisonResult imageComparisonResult = new ImageComparison(expectedImage, actualImage, resultDestination).compareImages();
	}
	
	public static void captureScreenshotspecific(WebElement ele) throws IOException
	{
		
		File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		BufferedImage  fullImg = ImageIO.read(screenshot);

		// Get the location of element on the page
		Point point = ele.getLocation();

		// Get width and height of the element
		int eleWidth = ele.getSize().getWidth();
		int eleHeight = ele.getSize().getHeight();

		// Crop the entire page screenshot to get only element screenshot
		BufferedImage eleScreenshot= fullImg.getSubimage(point.getX(), point.getY(),
		    eleWidth, eleHeight);
		ImageIO.write(eleScreenshot, "png", screenshot);

		// Copy the element screenshot to disk
		File screenshotLocation = new File("C:\\images\\GoogleLogo_screenshot.png");
		FileUtils.copyFile(screenshot, screenshotLocation);
	}

	public void incognitoMode(String url) {

		Map<String, Object> prefs = new HashMap<String, Object>();
		WebDriverManager.chromedriver().setup();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		prefs.put("credentials_enable_service", false);
		prefs.put("profile.password_manager_enabled", false);

		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		options.addArguments("--disable-extensions");
		options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
		options.setExperimentalOption("useAutomationExtension", false);
		options.addArguments("use-fake-ui-for-media-stream");
		options.addArguments("--incognito");
		driver = new ChromeDriver(options);
		// ngdriver = new NgWebDriver((JavascriptExecutor) driver);
		log.info("Chrome Browser launched");
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(CommonUtils.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(CommonUtils.IMPLICIT_WAIT, TimeUnit.SECONDS);
	}
}
