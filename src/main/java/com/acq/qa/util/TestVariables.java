/**
 * 
 */
package com.acq.qa.util;

import com.acq.integration.pages.SalesforceContacts;
import com.acq.qa.pages.HomePage;
import com.acq.qa.pages.LoginPage;
import com.github.javafaker.Faker;

/**
 * @author Acquire Team
 *
 */
public interface TestVariables {
	public static CommonUtils commonUtil = new CommonUtils();
	public static HomePage homePage = new HomePage();
	public static UserData uData = new UserData();
	public static LoginPage loginPage = new LoginPage();
	public static SalesforceContacts salesforce = new SalesforceContacts();
	public static Faker faker = new Faker();
	
}
