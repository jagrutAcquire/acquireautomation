package com.acq.integration.testcases;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.acq.qa.base.TestBase;
import com.acq.qa.util.CommonUtils;
import com.acq.qa.util.Variables;

public class SalesforceContactsTest extends TestBase {


	String lastName,email,phone,description,account;
	List<String> enterField,userChatMessage;  
	public SalesforceContactsTest() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
	}

	@Test(priority = 1, enabled = true)
	public void createCustomField() throws Exception {
		CommonUtils.enviroment(uData.getEnviroment());
		loginPage.login(uData.getUserEmail(), uData.getUserPassword());
		Assert.assertTrue(homePage.verifyUsername());
		commonUtil.deleteExistingField();
		commonUtil.createandEditCustomField();	
		commonUtil.scrollTillElement("dashUsername");
		commonUtil.waitforelement("dashUsername", 30);
		commonUtil.click("dashUsername");
		commonUtil.waitforelement("dashBtnLogout", 30);
		commonUtil.click("dashBtnLogout");
		
	}
	
	@Test(priority = 2, enabled = true)
	public void Mapcontact() throws Exception {
		CommonUtils.enviroment(uData.getEnviroment());
		loginPage.login(uData.getUserEmail(), uData.getUserPassword());
		Assert.assertTrue(homePage.verifyUsername());
		salesforce.clickappstoreMenu();
		commonUtil.verifyPageTitle("App Store");
		salesforce.clickAllCollection();
		commonUtil.verifyText("salesforceDisplay", "Salesforce");
		salesforce.clickSFSetting();
		salesforce.clickMappingFieldsMenu();
		salesforce.clickContact();
		commonUtil.checkFieldDisplayandMap("salesFName", "Name", "field-select-Name", "LastName", "Id");
		commonUtil.checkFieldDisplayandMap("salesFEmail", "Email", "field-select-Email", "Email", "Id");
		commonUtil.checkFieldDisplayandMap("salesFPhone", "Phone", "field-select-Phone", "Phone", "Id");
		commonUtil.checkFieldDisplayandMap("salesFDepartment", "Department", "field-select-Department", "Department","Id");
		commonUtil.checkFieldDisplayandMap("salesFRemark", "Remark", "field-select-Remark", "Description", "Id");
		commonUtil.checkFieldDisplayandMap("salesFAddress", "Address", "field-select-Address", "OtherStreet", "Id");
		commonUtil.checkFieldDisplayandMap("salesFAccount", "Account", "field-select-Account", "Account", "Id");
		commonUtil.mappContactFields("first-name", "FirstName");
		commonUtil.mappContactFields("salutation", "Salutation");
		commonUtil.mappContactFields("other-city", "OtherCity");
		commonUtil.mappContactFields("other-state", "OtherState");
		commonUtil.mappContactFields("other-postal-code", "OtherPostalCode");
		commonUtil.mappContactFields("other-country", "OtherCountry");
		commonUtil.mappContactFields("mailing-street", "MailingStreet");
		commonUtil.mappContactFields("mailing-city", "MailingCity");
		commonUtil.mappContactFields("mailing-state", "MailingState");
		commonUtil.mappContactFields("mailing-postal-code", "MailingPostalCode");
		commonUtil.mappContactFields("mailing-country", "MailingCountry");
		commonUtil.mappContactFields("mobile-phone", "MobilePhone");
		commonUtil.mappContactFields("fax", "Fax");
		commonUtil.mappContactFields("home-phone", "HomePhone");
		commonUtil.mappContactFields("other-phone", "OtherPhone");
		commonUtil.mappContactFields("assistant-phone", "AssistantPhone");
		commonUtil.mappContactFields("title", "Title");
		commonUtil.mappContactFields("assistant-name", "AssistantName");
		//commonUtil.mappContactFields("birthdate", "Birthdate");
		commonUtil.mappContactFields("level__c", "Level__c");
		commonUtil.mappContactFields("languages__c", "Languages__c");
		salesforce.confirmMapping();
		commonUtil.scrollTillElement("salesFSubtype");
		commonUtil.checkSFSyncSetting("salesFCase", "salesFCaseDrop","Chat Transcript-#Name",0);
		commonUtil.checkSFSyncSetting("salesFNote", "salesFNoteDrop","Chat Transcript-#Name",1);
		commonUtil.clickFeed("salesFFeed",2);
		commonUtil.scrollTillElement("salesFSaveSyncBtn");
		salesforce.clickSave();
		commonUtil.waitforelement("salesFToastMessage", 30);
		commonUtil.verifyTextwithTrim("salesFToastMessage", "MappedSuccessfully");
		commonUtil.scrollTillElement("dashUsername");
		commonUtil.waitforelement("dashUsername", 30);
		commonUtil.click("dashUsername");
		commonUtil.waitforelement("dashBtnLogout", 30);
		commonUtil.click("dashBtnLogout");
	}
	
	@Test(priority= 3, enabled= true)
	public void syncSalesforceData() throws Exception {
		CommonUtils.enviroment(uData.getEnviroment());
		commonUtil.loginToChatProcess();
		homePage.enterTextBackend("Backend");
		homePage.enterTextBackend("General, Issue , Testing");
		homePage.enterTextBackend("yes, say");
		homePage.enterTextBackend("How can i help");
		homePage.enterTextBackend("Trust is policy");
		homePage.enterTextBackend("Userdata behaviour");
		Thread.sleep(2000);
		userChatMessage = commonUtil.allchatMessages();
		homePage.clickProfileTab();
		lastName = faker.name().lastName();
		email = faker.name().fullName().replace(" ", "-")+"@gmail.com";
		phone = commonUtil.randomPhoneGenerate();
		description = faker.address().fullAddress();
		commonUtil.inputText("backendInName", lastName);
		Thread.sleep(1000);
		commonUtil.inputText("backendInEmail", email);
		Thread.sleep(1000);
		commonUtil.inputText("backendInPhone", phone);
		Thread.sleep(1000);
		commonUtil.inputText("backendInRemarks", description);
		List<String> customFields  = commonUtil.extractTag();
		customFields.remove(0);
		System.out.println("Final Custom Fields Need to add in Salesforce are : " +customFields);
		int cf = customFields.size();
		enterField = commonUtil.addSalesforceFields();
		//int ef = enterField.size();
		System.out.println("Total Add Field needs to be done" + enterField);
		System.out.println("Total Custom Field Size is: "+ cf);
		for (int i = 0; i < cf; i++) {
			commonUtil.inputTextWexpath(customFields.get(i), enterField.get(i));
		}
		account = faker.finance().bic();
		commonUtil.inputText("salesAccount", account);
		Thread.sleep(1000);
		commonUtil.scrollTillElement("backendSavetoCRMBtn");
		commonUtil.click("backendSavetoCRMBtn");
		Thread.sleep(3000);
		salesforce.clickSyncButton();
		commonUtil.waitforelement("wSFCSyncMessage", 30);
		commonUtil.verifyText("wSFCSyncMessage", "Chat detail exported successfully.");
		homePage.closeChatBackend();
		commonUtil.switchToMainFrame();
		commonUtil.switchframeByXpath("dashClosePopupIframe");
		commonUtil.click("dashBtnClosePopup");
		loginPage.clickLinkforLogout();
		loginPage.clickLogout();
	}

	@Test(priority = 4, enabled = true)
	public void checkSalesforce() throws Exception {
		
		//List<String> customFields  = commonUtil.extractTag();
		driver.get("https://login.salesforce.com/?locale=in");
		//commonUtil.switchTab("1");
		salesforce.Salesforcelogin(Variables.CR.getProperty("salesforceUN"), Variables.CR.getProperty("salesforcePWD"));
		commonUtil.verifyPageTitle("Home Page ~ Salesforce - Developer Edition");
		commonUtil.click("wSalesFContact");
		commonUtil.click("wSFCPopup");
		System.out.println("Total Element inList are  " + enterField);
		commonUtil.inputTextEnter("wSFSearchBox", lastName);
		Thread.sleep(5000);
		salesforce.sFContactLName(lastName);
		System.out.println("Full Displayed name is: "+enterField.get(2)+ " "+enterField.get(1)+" "+lastName);
		commonUtil.verifyText("wSFCIName", enterField.get(2)+ " "+enterField.get(1)+" "+lastName);
		commonUtil.verifyText("wSFCIEmail", email);
		commonUtil.verifyText("wSFCIPhone", phone);
		commonUtil.verifyText("wSFCIDescription", description);
		commonUtil.doubbleClick("wSFCIAddress");
		commonUtil.verifyText("wSFCAddressPopup", "Other Address");
		commonUtil.verifyTextFromInput("wSFCOtherCity", enterField.get(3));
		commonUtil.verifyTextFromInput("wSFCOtherState", enterField.get(4));
		commonUtil.verifyTextFromInput("wSFCOtherPostalCode", enterField.get(5));
		commonUtil.verifyTextFromInput("wSFCOthercountry", enterField.get(6));
		commonUtil.click("wSFCOkButton");
		commonUtil.doubbleClick("wSFCMailingAddress");
		commonUtil.verifyText("wSFCMailingPopup", "Mailing Address");
		commonUtil.verifyTextFromInput("wSFCMailingStreet", enterField.get(7));
		commonUtil.verifyTextFromInput("wSFCMailingCity",enterField.get(8));
		commonUtil.verifyTextFromInput("wSFCMailingState",enterField.get(9));
		commonUtil.verifyTextFromInput("wSFCMailingPostalCode", enterField.get(10));
		commonUtil.verifyTextFromInput("wSFCMailingCountry", enterField.get(11));
		commonUtil.click("wSFCOkButton");
		commonUtil.verifyText("wSFCFax", enterField.get(12));
		commonUtil.verifyText("wSFCMobilePhone", enterField.get(13));
		commonUtil.verifyText("wSFCHomePhone", enterField.get(14));
		commonUtil.verifyText("wSFCOtherPhone", enterField.get(15));
		commonUtil.verifyText("wSFCAssistantPhone", enterField.get(16));
		commonUtil.verifyText("wSFCTitle", enterField.get(17));
		commonUtil.verifyText("wSFCAssistantName", enterField.get(18));
		//for Birthdate
		commonUtil.verifyText("wSFCLevel_C", enterField.get(21));
		commonUtil.verifyText("wSFCLanguages_C", enterField.get(22));
		commonUtil.verifyText("wSFCAccount", account);
		//enterField.clear();
		//---------------check for CASE--------------------------
		commonUtil.scrollTillElement("wSFC_Case");
		commonUtil.verifyText("wSFC_Case", "Cases");
		commonUtil.salesforceCaseSubject(lastName);
		//commonUtil.salesforceNoteSubject(lastName);
		commonUtil.click("wSFCase_Detail_BTN");
		commonUtil.doubbleClick("wSFCase_Description");
		commonUtil.verifyText("wSFCase_Description_Popup", "Description");
		/*String allChatDescription = commonUtil.getallText("wSFCase_DescriptionText");
		System.out.println("allChatDescription BEFORE:: "+allChatDescription);*/
	
		/*String[] array = userChatMessage.toArray(new String[userChatMessage.size()]);
		System.out.println("Array Converted "+ Arrays.toString(array));
		*/
		/*if(userChatMessage.contains(allChatDescription)) {
			System.out.println("TRUE:");
			
		}else
		{
			System.out.println("TRUE:");
		}*/
		
		
	}

	@Test(priority = 5, enabled = false)
	public void checkCases() {
		
		
	}
	@AfterMethod
	public void tearDown() throws Exception {

		driver.quit();
		driver = null;

	}

}
