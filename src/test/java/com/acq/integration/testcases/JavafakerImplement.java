package com.acq.integration.testcases;

public class JavafakerImplement {
	
	
	private  String lastName;
	private  String email;
	private  String phone;
	private  String remarks;
	private  String accountid;
	private  String firstName;
	private  String salutation;
	private  String otherCity;
	private  String otherstate;
	private  String otherpostalCode;
	private  String otherCountry;
	private  String mailingStreet;
	private  String mailingCity;
	private  String mailingstate;
	private  String mailingPostalCode;
	private  String mailingCountry;
	private  String fax;
	private  String mobilePhone;
	private  String homePhone;
	private  String otherPhone;
	private  String assistantPhone;
	private  String title;
	private  String assistantName;
	private  String birthDate;
	private  String description;
	private  String level__c;
	private  String languages__c;
	private  String account;
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getAccountid() {
		return accountid;
	}
	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getOtherCity() {
		return otherCity;
	}
	public void setOtherCity(String otherCity) {
		this.otherCity = otherCity;
	}
	public String getOtherstate() {
		return otherstate;
	}
	public void setOtherstate(String otherstate) {
		this.otherstate = otherstate;
	}
	public String getOtherpostalCode() {
		return otherpostalCode;
	}
	public void setOtherpostalCode(String otherpostalCode) {
		this.otherpostalCode = otherpostalCode;
	}
	public String getOtherCountry() {
		return otherCountry;
	}
	public void setOtherCountry(String otherCountry) {
		this.otherCountry = otherCountry;
	}
	public String getMailingStreet() {
		return mailingStreet;
	}
	public void setMailingStreet(String mailingStreet) {
		this.mailingStreet = mailingStreet;
	}
	public String getMailingCity() {
		return mailingCity;
	}
	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}
	public String getMailingstate() {
		return mailingstate;
	}
	public void setMailingstate(String mailingstate) {
		this.mailingstate = mailingstate;
	}
	public String getMailingPostalCode() {
		return mailingPostalCode;
	}
	public void setMailingPostalCode(String mailingPostalCode) {
		this.mailingPostalCode = mailingPostalCode;
	}
	public String getMailingCountry() {
		return mailingCountry;
	}
	public void setMailingCountry(String mailingCountry) {
		this.mailingCountry = mailingCountry;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getOtherPhone() {
		return otherPhone;
	}
	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}
	public String getAssistantPhone() {
		return assistantPhone;
	}
	public void setAssistantPhone(String assistantPhone) {
		this.assistantPhone = assistantPhone;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAssistantName() {
		return assistantName;
	}
	public void setAssistantName(String assistantName) {
		this.assistantName = assistantName;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLevel__c() {
		return level__c;
	}
	public void setLevel__c(String level__c) {
		this.level__c = level__c;
	}
	public String getLanguages__c() {
		return languages__c;
	}
	public void setLanguages__c(String languages__c) {
		this.languages__c = languages__c;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	
	
	
}
