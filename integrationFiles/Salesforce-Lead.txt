<select _ngcontent-c8="" class="form-control" id="field-select-Name" name="user0">
   <option _ngcontent-c8="" value="">Ignore Column</option>
   <!---->
   <option _ngcontent-c8="" value="LastName">  Last Name </option>
   <option _ngcontent-c8="" value="FirstName">  First Name </option>
   <option _ngcontent-c8="" value="Salutation">  Salutation </option>
   <option _ngcontent-c8="" value="Title">  Title </option>
   <option _ngcontent-c8="" value="Company">  Company </option>
   <option _ngcontent-c8="" value="Street">  Street </option>
   <option _ngcontent-c8="" value="City">  City </option>
   <option _ngcontent-c8="" value="State">  State </option>
   <option _ngcontent-c8="" value="PostalCode">  Postal Code </option>
   <option _ngcontent-c8="" value="Country">  Country </option>
   <option _ngcontent-c8="" value="Phone">  Phone </option>
   <option _ngcontent-c8="" value="MobilePhone">  Mobile Phone </option>
   <option _ngcontent-c8="" value="Fax">  Fax </option>
   <option _ngcontent-c8="" value="Email">  Email </option>
   <option _ngcontent-c8="" value="Website">  Website </option>
   <option _ngcontent-c8="" value="Description">  Description </option>
   <option _ngcontent-c8="" value="Industry">  Industry </option>
   <option _ngcontent-c8="" value="Rating">  Rating </option>
   <option _ngcontent-c8="" value="AnnualRevenue">  Annual Revenue </option>
   <option _ngcontent-c8="" value="NumberOfEmployees">  Number Of Employees </option>
   <option _ngcontent-c8="" value="SICCode__c">  S I C Code__c </option>
   <option _ngcontent-c8="" value="ProductInterest__c">  Product Interest__c </option>
   <option _ngcontent-c8="" value="Primary__c">  Primary__c </option>
   <option _ngcontent-c8="" value="CurrentGenerators__c">  Current Generators__c </option>
   <option _ngcontent-c8="" value="NumberofLocations__c">  Numberof Locations__c </option>
</select>